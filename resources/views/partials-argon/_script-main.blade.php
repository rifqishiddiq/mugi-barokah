<!-- Argon Scripts -->
<!-- Core -->
<script src="{{asset('public/backend')}}/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/js-cookie/js.cookie.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/lavalamp/js/jquery.lavalamp.min.js"></script>
<!-- Optional JS -->
<script src="{{asset('public/backend')}}/assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/chart.js/dist/Chart.extension.js"></script>
<!-- Argon JS -->

<script src="{{asset('public/backend')}}/assets/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/datatables.net-select/js/dataTables.select.min.js"></script>

<script src="{{asset('public/backend')}}/assets/vendor/select2/dist/js/select2.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/nouislider/distribute/nouislider.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/quill/dist/quill.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/dropzone/dist/min/dropzone.min.js"></script>
<script src="{{asset('public/backend')}}/assets/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<script src="{{asset('public/backend')}}/assets/js/argon.min-v=1.0.0.js"></script>
<input type='hidden' name='_token' value='{{ csrf_token() }}'>

<script src="{{asset('public/backend/core/ajax.js')}}"></script>
<script src="{{asset('public/backend/core/datatable.js')}}"></script>

@yield('scripts')
<script>
    baseURL = '{{url("/")}}';
</script>
