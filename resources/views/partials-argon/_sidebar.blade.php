<style>
  a .navbar-brand{
    padding: 0px;
  }
  .navbar-brand-img{
    min-width: 8%;
    min-height: 8%;
    width: auto;
    height: auto;
    position: absolute;
    left: 15%;
    top: 6.3%;
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
  }
</style>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner" style="padding-top:2px">
      <!-- Brand -->
      <div class="sidenav-header d-flex align-items-center">
        @if(Session::get('activeUser')->user_role_id==0 || Session::get('activeUser')->user_role_id==1)
        <a class="navbar-brand" href="{{ url('daily-report') }}">
        @else
        <a class="navbar-brand" href="{{ url('schedule') }}">
        @endif
          <img src="{{asset('public/backend/')}}/assets/img/brand/mugibarokah-circle.png" class="navbar-brand-img" alt="...">
        </a>
        @if(Session::get('activeUser')->user_role_id==0 || Session::get('activeUser')->user_role_id==1)
        <a href="{{ url('daily-report') }}">
        @else
        <a href="{{ url('schedule') }}">
        @endif
        <h3 style="padding-left:22px; padding-top:7px"> MugiBarokah</h3>
        </a>
        <div class="ml-auto">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>

      <!-- <hr class="my-0" style> -->
      <!-- Heading -->
      <!-- <h6 class="navbar-heading p-0 text-muted">Documentation</h6> -->

      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            @if(Session::get('activeUser')->user_role_id==0 || Session::get('activeUser')->user_role_id==1)
            <li class="nav-item">
              <a class="nav-link" href="{{ url('daily-report')}}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-shop text-default"></i>
                <span class="nav-link-text">Laporan Harian</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('male')}}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-calendar-grid-58 text-blue"></i>
                <span class="nav-link-text">Jadwal Laki-laki</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('female')}}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-calendar-grid-58 text-pink"></i>
                <span class="nav-link-text">Jadwal Perempuan</span>
              </a>
            </li>
            @else
            <li class="nav-item">
              <a class="nav-link" href="{{ url('schedule')}}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-calendar-grid-58 text-info"></i>
                <span class="nav-link-text">Jadwal Klinik</span>
              </a>
            </li>
            @endif
            @if(Session::get('activeUser')->user_role_id==0 || Session::get('activeUser')->user_role_id==1)
            <li class="nav-item">
              <a class="nav-link" href="{{url('chart')}}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-chart-bar-32 text-info"></i>
                <span class="nav-link-text">Grafik Bulanan</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('queue')}}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-book-bookmark text-purple"></i>
                <span class="nav-link-text">Rekap Antrian</span>
              </a>
            </li>
            @else
            <li class="nav-item">
              <a class="nav-link" href="{{ url('queue')}}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-book-bookmark text-pink"></i>
                <span class="nav-link-text">Pesanan Antrian</span>
              </a>
            </li>

            @endif
            <li class="nav-item">
              <a class="nav-link" href="{{ url('medical-record')}}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-single-copy-04 text-success"></i>
                <span class="nav-link-text">Rekam Medis</span>
              </a>
            </li>
            @if(Session::get('activeUser')->user_role_id==0 || Session::get('activeUser')->user_role_id==1)
            <li class="nav-item">
              <a class="nav-link" href="{{ url('patient') }}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-circle-08 text-warning"></i>
                <span class="nav-link-text">Klien</span>
              </a>
            </li>
            @endif
            @if(Session::get('activeUser')->user_role_id==0)
            <li class="nav-item">
              <a class="nav-link" href="{{ url('employee') }}" role="button" aria-controls="navbar-dashboards">
                <i class="ni ni-single-02 text-primary"></i>
                <span class="nav-link-text">Pegawai</span>
              </a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link" href="{{ url('logout') }}">
                <i class="ni ni-button-power text-danger"></i> Logout
              </a>
            </li>
          </ul>
          <!-- Divider -->
          <!-- <hr class="my-3"> -->
          <!-- Heading -->
          <!-- <h6 class="navbar-heading p-0 text-muted">Documentation</h6> -->
          <!-- Navigation -->
        </div>
      </div>
    </div>
  </nav>
