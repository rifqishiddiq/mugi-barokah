            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <?php
                  $id = Session::get('activeUser')->id;
                  $id = Crypt::encrypt($id);
                ?>
                <a href="{{ url('user', $id)}}">
                  <img src="{{ asset('public/backend/assets/images/Admin-icon.png') }}" alt="..." class="img-circle profile_img">
                </a>
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <a href="{{ url('user', $id)}}">
                  <h2>{{Session::get('activeUser')->user_username}}</h2>
                </a>
              </div>
            </div>
            <!-- /menu profile quick info -->
