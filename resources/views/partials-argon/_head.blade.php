<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Mugi Barokah - @yield('title')</title>
    <!-- Favicon -->
    <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah-circle.png" type="image/png">
    <!-- <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah2.PNG" type="image/png"> -->
    <!-- <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah.PNG" type="image/png"> -->
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="{{asset('public/backend')}}/assets/vendor/nucleo/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="{{asset('public/backend')}}/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
    <!-- Page plugins -->
    <!-- Argon CSS -->
    <link rel="stylesheet" href="{{asset('public/backend')}}/assets/css/argon.min-v=1.0.0.css" type="text/css">
    <link rel="stylesheet" href="{{asset('public/backend')}}/core/ajax.css" type="text/css">
    <link rel="stylesheet" href="{{asset('public/backend')}}/assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('public/backend')}}/assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('public/backend')}}/assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
    <!-- Page plugins -->
    <link rel="stylesheet" href="{{asset('public/backend')}}/assets/vendor/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('public/backend')}}/assets/vendor/quill/dist/quill.core.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        /* .form-control{
            font-size: .675rem;
        } */
        tr:nth-child(even) {background-color: #f2f2f2;}

        #modal-header{
            padding-bottom: 0px;
        }
        #modal-output{
            font-family: inherit;
            font-size: 12px;
            padding-top:0px;
        }
    </style>

</head>