<<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Mugi Barokah - Login</title>

    <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah-circle.png" type="image/png">
    <!-- <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah2.PNG" type="image/png"> -->
    <!-- <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah.PNG" type="image/png"> -->

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <main class="py-4">

            <div class="container" style="margin-top: 60px;">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">

                            <div class="card-body">
                            <br><br><br>
                                <form method="POST" action="{{ url('/register-save') }}">
                                    @csrf

                        <div class='form-group'>
                            <label for='nama' class='col-sm-5 col-xs-12 control-label'>Nama Lengkap</label>
                            <div class='col-sm-7 col-xs-12'>
                                <input type="text" name="patient_fullname" class="form-control" value="{{$patient->patient_fullname}}" required>
                            </div>
                        </div>

                        <div class='form-group'>
                            <label for='nama' class='col-sm-5 col-xs-12 control-label'>Jenis Kelamin</label>
                            <div class='col-sm-9 col-xs-12'>
                                <input type="radio" name="patient_gender" value="male"> Male <br>
                                <input type="radio" name="patient_gender" value="female" required> Female
                            </div>
                        </div>

                        <div class='form-group'>
                            <label for='nama' class='col-sm-5 col-xs-12 control-label'>Nomor Telepon / HP</label>
                            <div class='col-sm-5 col-xs-12'>
                                <input type="number" name="patient_phone_number" class="form-control" value="" required>
                            </div>
                        </div>

                        <br>
                        
                        @if(!is_null($user->user_username))
                        <input type='hidden' name='ada' value='{{  $user->user_username }}'>

                        @else
                        <div class='form-group'>
                            <label for='nama' class='col-sm-5 col-xs-12 control-label'>Username</label>
                            <div class='col-sm-7 col-xs-12'>
                                <input type="text" name="user_username" class="form-control" value="{{$user->user_username}}" required>
                            </div>
                        </div>

                        <div class='form-group'>
                            <label for='nama' class='col-sm-5 col-xs-12 control-label'>Password</label>
                            <div class='col-sm-7 col-xs-12'>
                                <input type="password" name="user_password" class="form-control" value="{{$user->user_password}}" required>
                            </div>
                        </div>
                        @endif

                        <div class='form-group'>
                            <label class='col-sm-5 col-xs-12 control-label'></label>
                            <div class='col-sm-9 col-xs-12'>
                                <input type="submit" class="btn btn-primary" value="Submit">
                            </div>
                        </div>

                        <input type='hidden' name='user_id' value='{{  encrypt($user->id) }}'>
                        <input type='hidden' name='user_role_id' value='2'>
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <!-- </center> -->
                                </form>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>

        </main>
    </div>

    <script>
    $(document).ready( function($) {
        // Disable scroll when focused on a number input.
        $('form').on('focus', 'input[type=number]', function(e) {
            $(this).on('wheel', function(e) {
                e.preventDefault();
            });
        });
        // Restore scroll on number inputs.
        $('form').on('blur', 'input[type=number]', function(e) {
            $(this).off('wheel');
        });
        // Disable up and down keys.
        $('form').on('keydown', 'input[type=number]', function(e) {
            if ( e.which == 38 || e.which == 40 )
                e.preventDefault();
        });      

        $('form').on('focus', 'input[type=number]', function(e) {
            // Remove invalid characters
            var sanitized = $(this).val().replace(/[^-0-9]/g, '');
            // Remove non-leading minus signs
            sanitized = sanitized.replace(/(.)-+/g, '$1');
            // Update value
            $(this).val(sanitized);
        });
    });

    </script>

</body>
</html>
