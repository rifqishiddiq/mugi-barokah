<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="329160013724-joi816ffoai1483d3npljt8qumhn28pv.apps.googleusercontent.com">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Mugi Barokah - Login</title>

    <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah-circle.png" type="image/png">
    <!-- <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah2.PNG" type="image/png"> -->
    <!-- <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah.PNG" type="image/png"> -->

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}" defer></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <main class="py-4">

            <div class="container" style="margin-top: 60px;">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="card">

                            <div class="card-body">
                            <br><br><br>
                                <form method="POST" action="{{ url('/validate-login') }}">
                                    @csrf
                                    <center>
                                        <input placeholder="username" id="username" type="text" style="width: 260px; margin-bottom: 10px;" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                                        @if ($errors->has('username'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif                                

                                        <input placeholder="Password" id="password" type="password" style="width: 260px; margin-bottom: 20px;" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif

                                        <button type="submit" class="btn btn-primary" style="margin-bottom: 30px; width: 260px;">
                                            Login
                                        </button>
                                    </center>
                                </form>
                                <center>
                                <a href="{{ url('register')}}" style="text-align:center">register here</a>
                                </center>
                                <br />
                                <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6" style="padding: 0 2px">
                                                <div class="g-signin2" data-onsuccess="onSignIn" align="right"></div>
                                            </div>
                                            <div class="col-md-6" style="padding: 0 2px">
                                                <a href="{{url('/auth/facebook')}}" class="btn btn-primary" align="left">Login with Facebook</a>
                                            </div>
                                        </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>
    </div>

<!-- <script>
function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}
</script> -->
</body>
</html>