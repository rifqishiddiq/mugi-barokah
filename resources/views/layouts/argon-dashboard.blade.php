<!DOCTYPE html>
<html>

@include('partials-argon._head')

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!-- Sidenav -->
  @include('partials-argon._sidebar')            

  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    @include('partials-argon._top-nav')            

    <!-- Content -->
    @yield('content')

    <!-- Footer -->
    {{--    @include('partials-argon._footer')             --}}

  </div>
  <!-- Argon Scripts -->
  <!-- Core(Script) -->
  @include('partials-argon._script-main')            

</body>

</html>