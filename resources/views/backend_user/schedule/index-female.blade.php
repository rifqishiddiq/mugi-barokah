@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
<style>
td {
    overflow: hidden;
    width:auto;
}
td a {
    display: block;
    margin: -10em;
    padding: 10em;
}
.cancel{
    display: inline;
    margin: 0;
    padding: 0;
}
</style>

<div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Home</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Jadwal Klinik</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3>Jadwal Klinik : {{date('l, j F Y')}}</h3>
            </div>
            <div class="col-md-6">
              <div align="right">
                <a onclick="loadModal(this)" target="/queue/add" title="Pesan Antrian">
                  <button align="right" type="button" class="btn bg-success text-white">  
                    <i class="glyphicon glyphicon-plus"></i>
                    Pesan Antrian
                  </button>
                </a>
              </div>
            </div>
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">


                    <!-- @if(is_null($booked))
                    <h4 style="margin-left:0" align="center">click the row on the table to book/reserve</h4>
                    @else
                    <h4 style="margin-left:0" align="center">click the row on the table to <b style="color:blue">change</b> your book/reserve</h5>
                    @endif -->
                    <h5 style="margin-left:0;" align="center">*hanya bisa booking 1 sesi saja</h4>

                    <table class="table table-bordered table-striped" style="text-align: center; width: 50%;" align="center" id="table-role">
                      <thead class="thead thead-light thead-dark" style="">
                        <tr class="headings">
                          <!-- <th class="column-title" style="text-align: center; width: 40px">No. </th> -->
                          <th class="column-title" style="text-align: center;">Dipesan oleh </th>
                          <th class="column-title" style="text-align: center;">Jam </th>
                          <th class="column-title" style="text-align: center;">Aksi </th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php $cetak = "-"; ?>
                        @foreach($time as $num => $t)
                        <tr>
                          <!-- <td style="padding-left: 40px; padding-top: 20px; padding-bottom: 0px"  >{{$num+1}}</td> -->
                          @foreach($today_female as $tq)
                            @if(strtotime($t) == strtotime($tq->queue_time))
                              @if($tq->queue_patient_id == $patient_data->id)
                              <?php $cetak = "dipesan oleh anda" ?>
                              @else
                              <?php $cetak = "dipesan orang lain" ?>
                              @endif
                            @endif
                          @endforeach

                          @if($cetak != "-")
                          @if($cetak=="dipesan orang lain")
                          <td>{{$cetak}}</td>
                          @else
                          <td style="color:blue">{{$cetak}}</td>
                          @endif
                          <td>{{$t}}</td>
                          <td>
                          @if(is_null($booked))
                            -
                          @elseif(strtotime($booked->queue_time)!=strtotime($t))
                            -
                          @else
                          @if($booked->queue_status_check==2)
                            <a class="cancel" onclick="deleteData( '{{encrypt($booked->id)}}' )" title="Cancel" style="color: red">
                              batalkan
                            </a>
                          @else
                            -
                          @endif                            
                          @endif
                          </td>                            
                          @else
                          <td>
                            <!-- <a onclick="addData({{strtotime($t)}})" title="Reserve" style="display: block; text-decoration: none;"> -->
                              {{$cetak}}
                            <!-- </a> -->
                          </td>
                          <td>
                            <!-- <a onclick="addData({{strtotime($t)}})" title="Reserve" style="display: block; text-decoration: none;"> -->
                              {{$t}}
                            <!-- </a> -->
                          </td>
                          <td>
                            <!-- <a onclick="addData({{strtotime($t)}})" title="Reserve" style="display: block; text-decoration: none;"> -->
                              -
                            <!-- </a> -->
                          </td>

                          @endif
                          <?php $cetak = "-"; ?>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>



  <script>
    function addData(datetime) {
      var data = new FormData();
      data.append('datetime', datetime);

      var datetime = new Date(datetime*1000);
      var hours = datetime.getHours();
      var minutes = datetime.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;

      modalConfirm("Konfirmasi", `Apakah anda yakin akan memesan pada jam ${strTime} ?`, function () {
        ajaxTransfer("/queue/save", data, "#modal-output");
      })
    }

    function deleteData(id) {
      var data = new FormData();
      data.append('id', id);

      modalConfirm("Konfirmasi", "Apakah anda yakin akan Membatalkan reservasi?", function () {
        ajaxTransfer("/queue/delete", data, "#modal-output");
      })
    }

  </script>
@stop

