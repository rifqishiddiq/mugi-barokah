<!DOCTYPE html>
<html>
<head>
	<title>Rekam Medis PDF</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="icon" href="{{asset('public/backend')}}/assets/img/brand/mugibarokah-circle.png" type="image/png">
    <link rel="stylesheet" href="{{asset('public/backend')}}/assets/css/argon.min-v=1.0.0.css" type="text/css">
    <style>
        tr:nth-child(even) {background-color: #f2f2f2;}

		tr{
			padding-top:10px;
			padding-bottom:10px;
		}

		tr td {
			padding-top:10px;
			padding-bottom:10px;
		}

		.navbar-brand{
			padding-top: 20px;
		}
		
		.navbar-brand-img{
			width: 160px;
			height: 160px;
		}
    </style>

</head>
<body style="background-color:white; background:white">
<div align="center" style="padding-top:12px">
	<img src="{{asset('public/backend')}}/assets/img/brand/mugibarokah-circle.png" type="image/png" class="navbar-brand-img">
	<br>
	<h1 class="text-center">RUMAH BEKAM MUGIBAROKAH</h1>
	<h3 class="text-center">Jl. Gayungan Pasar No.24 Surabaya 60235 Jawa Timur</h3>

</div>
<br>
	<table class="table" style="border:0px">
		<tr>
			<td style="width:140px"> <b>ID Resi:</b> </td>
			<td> {{$therapy_recap->id}} </td>
		</tr>
		<tr>
			<td> <b>Nama Lengkap Klien:</b> </td>
			<td> {{$queue->getPatient->patient_fullname}} </td>
		</tr>
		<tr>
			<td> <b>ID Medis:</b> </td>
			<td> {{$queue->getPatient->patient_medical_id}}  </td>
		</tr>
		<tr>
			<td> <b>Keluhan:</b> </td>
			<td> {{$therapy_recap->therapy_recap_complaint}}  </td>
		</tr>
		<tr>
			<td> <b>Jenis Terapi:</b> </td>
			<td> {{$therapy_recap->getTherapy->therapy_name}}  </td>
		</tr>
		<tr>
			<td> <b>Terapist:</b> </td>
			<td> {{$therapy_recap->getTherapist->employee_name}}  </td>
		</tr>
		<tr>
			<td> <b>Resep:</b> </td>
			<td> {{$therapy_recap->therapy_recap_receipt}}  </td>
		</tr>
		<tr>
			<td> <b>Hasil Pemeriksaan:</b> </td>
			<td> {{$therapy_recap->therapy_recap_result}}  </td>
		</tr>
		<tr>
			<td> <b>Harga Terapi:</b> </td>
			<td> Rp {{ number_format($therapy_recap->therapy_recap_price,2,',','.') }}  </td>
		</tr>
		<tr>
			<td> <b>Subsidi:</b> </td>
			<td> Rp {{ number_format($therapy_recap->therapy_recap_subsidy,2,',','.') }}  </td>
		</tr>
		<tr>
			<td> <b>Harga Total:</b> </td>
			<td> Rp {{ number_format($therapy_recap->therapy_recap_total_price,2,',','.') }}  </td>
		</tr>
	</table>
    <p>*harga yg tertera belum termasuk pembelian obat</p>


</body>
</html>