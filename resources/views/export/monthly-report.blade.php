<table>
	<thead>
    <b>
        <tr>
			<th> No </th>
			<th> ID Rekam Data </th>
			<th> Nama Klien</th>
			<th> ID Rekam Klien </th>
			<th> No HP </th>
			<th> Alamat </th>
			<th> Pendidikan Terakhir </th>
			<th> Keluhan </th>
			<th> Jenis Terapi </th>
			<th> Tanggal </th>
        </tr>
    </b>
    </thead>
    <tbody>
    @php $no = 1 @endphp
    @foreach($data as $key => $value)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $value->id }}</td>
            <td>{{ $value->patient_fullname }}</td>
            <td>{{ $value->patient_medical_id }}</td>
            <td>{{ $value->patient_phone_number }}</td>
            <td>{{ $value->patient_address }}</td>
            <td>{{ $value->last_education_name }}</td>
            <td>{{ $value->therapy_recap_complaint }}</td>
            <td>{{ $value->therapy_name }}</td>
            <td>{{ $value->queue_date }}-{{ $value->queue_time }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
