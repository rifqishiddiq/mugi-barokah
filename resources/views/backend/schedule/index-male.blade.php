@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')

<style>
td{
  width: auto;
}
</style>
    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 d-inline-block mb-0 text-white">Home</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Jadwal Laki-laki</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3>Jadwal Klinik : {{date('l, j F Y')}}</h3>
            </div>
            <div class="col-md-6">
              <div align="right">
                <a onclick="loadModal(this)" target="/queue/add" data="gender=2"title="Pesan Antrian">
                  <button align="right" type="button" class="btn bg-success text-white">  
                    <i class="glyphicon glyphicon-plus"></i>
                    Pesan Antrian
                  </button>
                </a>
              </div>
            </div>
          </div>

        <!-- Card body -->
        <div class="card-body">

                    <table class="table table-bordered table-striped" style="text-align: center" align="center" id="table-role">
                      <thead class="thead thead-light thead-dark">
                        <tr class="headings">
                          <!-- <th class="column-title" style="text-align: center; width: 40px">No. </th> -->
                          <th class="column-title" style="text-align: center; ">Dipesan oleh</th>
                          <!-- <th class="column-title" style="text-align: center;">Patient id </th> -->
                          <th class="column-title" style="text-align: center;">Status </th>
                          <th class="column-title" style="text-align: center;">Jam </th>
                          <th class="column-title" style="text-align: center;">Aksi </th>
                        </tr>
                      </thead>

                      <tbody class=" ">
                        <?php
                          $cetak = "-";
                          $id = null;
                          $status = null;
                          $action = null;
                          $approved = null;
                        ?>
                          @foreach($time as $num => $t)
                          <tr>
                            <!-- <td style="padding-left: 40px; padding-top: 20px; padding-bottom: 0px"  >{{$num+1}}</td> -->
                            @foreach($today_male as $tq)

                              @if(strtotime($t) == strtotime($tq->queue_time))
                              @foreach($today_queue as $today)
                              @if($tq->id==$today->queue_patient_id)
                              <?php
                                $cetak = $tq->getPatient->patient_fullname;
                                $id = $today->id;
                                $action = "ada";
                              ?>
                              @endif
                              @endforeach
                              @if($tq->queue_status_check == 2)
                              <?php
                                $status = 2;
                              ?>
                              @elseif($tq->queue_status_check == 1)
                              <?php
                                $status = 1;
                                $approved = 1;
                                ?>

                              @elseif($tq->queue_status_check == 3)
                              <?php
                                $status = 3;
                                $approved = 3;
                                ?>

                              @endif

                              @endif

                            @endforeach
                            <td style="max-width:300px">{{$cetak}}</td>

                            <!-- @if($cetak=="-")
                            <td>{{$cetak}}</td>
                            @else
                            <td>{{$tq->id}}</td>
                            @endif -->

                            @if(is_null($status))
                            <td>-</td>
                            @elseif($status==2)
                            <td>belum diproses</td>
                            @elseif($status==1)
                            <td class="text-success">selesai</td>
                            @else
                            <td class="text-primary">sedang diproses</td>
                            @endif

                            <td>{{$t}}</td>

                            @if(is_null($action))
                            <td>-</td>
                            @else

                            @if(is_null($approved))
                            <td>
                              <a class="btn btn-sm btn-primary text-secondary" onclick="processData({{$id}})" title="Process">
                              <span class="btn-inner--text" style="text-align: center; padding: 0">Proses</span></a>
                              <a class="btn btn-sm btn-danger text-secondary" onclick="deleteData( '{{encrypt($id)}}' )" title="Delete">
                              <span class="btn-inner--text" style="text-align: center; padding: 0">Delete</span></a>
                            </td>
                            @elseif($approved==3)
                            <td>
                              <!-- <a class="btn btn-sm btn-success text-secondary" onclick="approveData({{$id}})" title="Approve">
                              <span class="btn-inner--text" style="text-align: center; padding: 0">Approve</span></a> -->
                              <a class="btn btn-sm btn-success text-secondary" href="{{url('queue/detail', encrypt($id))}}" title="Detail">
                              <span class="btn-inner--text" style="text-align: center; padding: 0">Detail</span></a>
                              <a class="btn btn-sm btn-warning text-secondary" onclick="declineData({{$id}})" title="Decline">
                              <span class="btn-inner--text" style="text-align: center; padding: 0">Batalkan</span></a>
                              <a class="btn btn-sm btn-danger text-secondary" onclick="deleteData( '{{encrypt($id)}}' )" title="Delete">
                              <span class="btn-inner--text" style="text-align: center; padding: 0">Delete</span></a>
                            </td>

                            @else
                            <td>-</td>
                            @endif

                            @endif

                            <?php
                              $cetak = "-";
                              $id = null;
                              $status = null;
                              $action = null;
                              $approved = null;
                              ?>
                          </tr>
                          @endforeach
                      </tbody>
                    </table>

        </div>
      </div>
    </div>
  
    @stop
    
    @section('scripts')

    <script>
    function approveData(id) {
      var data = new FormData();
      data.append('id', id);

      modalConfirm("Konfirmasi", "Apakah anda yakin akan menyetujui pemesanan ini ?", function () {
        ajaxTransfer("/queue/approve", data, "#modal-output");
      })
    }

    function processData(id) {
      var data = new FormData();
      data.append('id', id);

      modalConfirm("Konfirmasi", "Apakah anda yakin akan memproses pemesanan ini ?", function () {
        ajaxTransfer("/queue/process", data, "#modal-output");
      })
    }

    function declineData(id) {
      var data = new FormData();
      data.append('id', id);

      modalConfirm("Konfirmasi", "Apakah anda yakin akan membatalkan pemrosesan pemesanan ini ?", function () {
        ajaxTransfer("/queue/decline", data, "#modal-output");
      })
    }

    function deleteData(id) {
      var data = new FormData();
      data.append('id', id);

      modalConfirm("Konfirmasi", "Apakah anda yakin akan Membatalkan reservasi?", function () {
        ajaxTransfer("/queue/delete", data, "#modal-output");
      })
    }

  </script>

@stop

