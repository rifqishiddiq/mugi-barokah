@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Tabel Klien</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active"><a href="#" style="color:#dee2e6">Klien</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Form Klien</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">Form Klien</h3>
            </div>
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">

        <form method="POST" action="{{ url('/patient/save') }}" class='form-horizontal'>
 
        @if( Session::get('activeUser')->user_role_id!=2 )
            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>ID Medis</label>
                <div class='col-sm-5 col-xs-12'>
                    <input type="text" name="patient_medical_id" class="form-control" value="{{$patient->patient_medical_id}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Nama Lengkap</label>
                <div class='col-sm-7 col-xs-12'>
                    <input type="text" name="patient_fullname" class="form-control" value="{{$patient->patient_fullname}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Nama Panggilan</label>
                <div class='col-sm-4 col-xs-12'>
                    <input type="text" name="patient_nickname" class="form-control" value="{{$patient->patient_nickname}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Jenis Kelamin</label>
                <div class='col-sm-9 col-xs-12'>
                    @if(!is_null($patient->patient_gender))
                        @if($patient->patient_gender=='laki_laki')
                        <input type="radio" name="patient_gender" value="laki_laki" checked> laki laki <br>
                        <input type="radio" name="patient_gender" value="perempuan" required> perempuan
                        @else
                        <input type="radio" name="patient_gender" value="laki_laki"> laki laki <br>
                        <input type="radio" name="patient_gender" value="perempuan" required checked> perempuan
                        @endif
                    @else
                    <input type="radio" name="patient_gender" value="laki_laki"> laki laki <br>
                    <input type="radio" name="patient_gender" value="perempuan" required> perempuan
                    @endif
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Tempat Lahir</label>
                <div class='col-sm-5 col-xs-12'>
                    @if(is_null($patient->patient_regency_id))
                    <input type="text" list="patient_regency_id" id="birthplace" name="birthplace" class="form-control" placeholder="choose your city birhtplace" value="" required>
                    @else
                    <input type="text" list="patient_regency_id" id="birthplace" name="birthplace" class="form-control" placeholder="choose your city birhtplace" value="{{$patient->getRegency->regency_name}}" required>
                    @endif
                    <datalist id="patient_regency_id" style="display:none">
                    <select>
                        @foreach($regencyData as $r)
                        <option data-value="{{$r->id}}">{{$r->regency_name}}</option>
                        @endforeach
                    </select>
                    </datalist>
                    <input type="hidden" name="patient_regency_id" id="birthplace-hidden">            
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Pendidikan Terakhir</label>
                <div class='col-sm-5 col-xs-12'>
                    @foreach($last_education as $l)
                    @if(is_null($patient->patient_last_education_id))
                        <input type="radio" name="patient_last_education_id" value="{{$l->id}}"> {{$l->last_education_name}} <br>
                    @else
                        @if($patient->getLastEducation->id == $l->id)
                            <input type="radio" name="patient_last_education_id" value="{{$l->id}}" checked> {{$l->last_education_name}} <br>
                        @else
                            <input type="radio" name="patient_last_education_id" value="{{$l->id}}"> {{$l->last_education_name}} <br>
                        @endif
                    @endif
                    @endforeach
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Pekerjaan</label>
                <div class='col-sm-5 col-xs-12'>
                    <input type="text" name="patient_job" class="form-control" value="{{$patient->patient_job}}" required>
                </div>
            </div>


            <div class="form-group">
                <label for="deadline" class='col-sm-5 col-xs-12 control-label'>Tanggal Lahir</label>
                <div class='col-sm-3 col-xs-12'>
                <input type="date" name="patient_birth_date" class="form-control" value="{{$patient->patient_birth_date}}" max="{{date('dd/mm/yyyy')}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Nomor Telepon / HP</label>
                <div class='col-sm-5 col-xs-12'>
                    <input type="number" name="patient_phone_number" class="form-control" value="{{$patient->patient_phone_number}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Nomor Telepon / HP (2) <span class="text-gray">(optional)</span></label>
                <div class='col-sm-5 col-xs-12'>
                    <input type="number" name="patient_phone_number_2" class="form-control" value="{{$patient->patient_phone_number_2}}">
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Email</label>
                <div class='col-sm-9 col-xs-12'>
                    <input type="email" name="patient_email" class="form-control" value="{{$patient->patient_email}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Alamat</label>
                <div class='col-sm-9 col-xs-12'>
                    @if(!is_null($patient->patient_address))
                    <textarea name="patient_address" class="form-control" value="{{$patient->patient_address}}" required>{{$patient->patient_address}}</textarea>
                    @else
                    <textarea name="patient_address" class="form-control" value="{{$patient->patient_address}}" required></textarea>
                    @endif
                </div>
            </div>

            
            <div class='form-group'>
                <div class="column">
                    <label for='nama' class='col-sm-5 col-xs-12 control-label'>Agama</label>
                    <div class='col-sm-4 col-xs-12'>
                        @foreach($religion as $r)
                        @if($patient->patient_religion == $r)
                        <input type="radio" name="patient_religion" value="{{$r}}" checked> {{$r}} <br>
                        @else
                        <input type="radio" name="patient_religion" value="{{$r}}"> {{$r}} <br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <div class='form-group'>
                <div class="column">
                    <label for='nama' class='col-sm-5 col-xs-12 control-label'>Status Perkawinan</label>
                    <div class='col-sm-4 col-xs-12'>
                        @foreach($mariage_status as $m)
                        @if($patient->patient_mariage_status == $m)
                        <input type="radio" name="patient_mariage_status" value="{{$m}}" checked> {{$m}} <br>
                        @else
                        <input type="radio" name="patient_mariage_status" value="{{$m}}"> {{$m}} <br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <div class='form-group'>
                <div class="column">
                    <label for='nama' class='col-sm-5 col-xs-12 control-label'>Info Mugi Barokah</label>
                    <div class='col-sm-9 col-xs-12'>
                        @foreach($source_info as $s)
                        @if($patient->patient_source_info == $s)
                        <input type="radio" name="patient_source_info" value="{{$s}}" checked> {{$s}} <br>
                        @else
                        <input type="radio" name="patient_source_info" value="{{$s}}"> {{$s}} <br>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Riwayat Penyakit</label>
                <div class='col-sm-9 col-xs-12'>
                    <textarea name="patient_allergic_history" class="form-control" value="{{$patient->patient_allergic_history}}"></textarea>
                </div>
            </div>

            <br>
            @else
            <input type="hidden" name="patient_medical_id" class="form-control" value="{{$patient->patient_medical_id}}">
            <input type="hidden" name="patient_fullname" class="form-control" value="{{$patient->patient_fullname}}">
            <input type="hidden" name="patient_nickname" class="form-control" value="{{$patient->patient_nickname}}">
            <input type="hidden" name="patient_gender" class="form-control" value="{{$patient->patient_gender}}">
            <input type="hidden" name="birthplace" class="form-control" value="{{$patient->getRegency->regency_name}}">
            <input type="hidden" name="patient_regency_id" class="form-control" value="{{$patient->patient_regency_id}}">            
            <input type="hidden" name="patient_job" class="form-control" value="{{$patient->patient_job}}">
            <input type="hidden" name="patient_birth_date" class="form-control" value="{{$patient->patient_birth_date}}">
            <input type="hidden" name="patient_email" class="form-control" value="{{$patient->patient_email}}">
            <input type="hidden" name="patient_address" class="form-control" value="{{$patient->patient_address}}">
            <input type="hidden" name="patient_phone_number" class="form-control" value="{{$patient->patient_phone_number}}">
            <input type="hidden" name="patient_phone_number_2" class="form-control" value="{{$patient->patient_phone_number_2}}">
            <input type="hidden" name="patient_religion" class="form-control" value="{{$patient->patient_religion}}">
            <input type="hidden" name="patient_mariage_status" class="form-control" value="{{$patient->patient_mariage_status}}">
            <input type="hidden" name="patient_source_info" class="form-control" value="{{$patient->patient_source_info}}">
            <input type="hidden" name="patient_allergic_history" class="form-control" value="{{$patient->patient_allergic_history}}">

            @endif

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Username</label>
                <div class='col-sm-9 col-xs-12'>
                    <input type="text" name="user_username" class="form-control" value="{{$user->user_username}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Password</label>
                <div class='col-sm-9 col-xs-12'>
                    <input type="password" name="user_password" class="form-control" value="{{$user->user_password}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label class='col-sm-5 col-xs-12 control-label'></label>
                <div class='col-sm-9 col-xs-12'>
                    <input type="submit" class="btn btn-success" value="Add Patient">
                </div>
            </div>

            <input type='hidden' name='user_id' value='{{  encrypt($user->id) }}'>
            <input type='hidden' name='user_role_id' value='2'>
            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
            </form>


        </div>
      </div>
    </div>

  @stop

  @section('scripts')

  <script>
    $(document).ready( function($) {
        // Disable scroll when focused on a number input.
        $('form').on('focus', 'input[type=number]', function(e) {
            $(this).on('wheel', function(e) {
                e.preventDefault();
            });
        });
        // Restore scroll on number inputs.
        $('form').on('blur', 'input[type=number]', function(e) {
            $(this).off('wheel');
        });
        // Disable up and down keys.
        $('form').on('keydown', 'input[type=number]', function(e) {
            if ( e.which == 38 || e.which == 40 )
                e.preventDefault();
        });      

        $('form').on('focus', 'input[type=number]', function(e) {
            // Remove invalid characters
            var sanitized = $(this).val().replace(/[^-0-9]/g, '');
            // Remove non-leading minus signs
            sanitized = sanitized.replace(/(.)-+/g, '$1');
            // Update value
            $(this).val(sanitized);
        });
    });

    document.querySelector("input[list='patient_regency_id']").addEventListener('input', function(e) {
    var input = e.target,
        list = input.getAttribute('list'),
        options = document.querySelectorAll('#' + list + ' select option'),
        hiddenInput = document.getElementById(input.id + '-hidden'),
        inputValue = input.value;

    hiddenInput.value = options.value;

    for(var i = 0; i < options.length; i++) {
        var option = options[i];

        if(option.innerText === inputValue) {
            hiddenInput.value = option.getAttribute('data-value');
            break;
        }
    }
    });

  </script>
  @stop