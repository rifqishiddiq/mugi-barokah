@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Tabel Klien</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active"><a href="#" style="color:#dee2e6">Klien</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Detail Klien</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">Detail Klien</h3>
            </div>
            <div class="col-md-6">
              <div align="right">
                <a href="{{ url('/patient/add', $patient->patient_user_id) }}">
                  <button align="right" type="button" class="btn bg-success text-white">  
                    <i class="glyphicon glyphicon-plus"></i>
                    Edit Klien
                  </button>
                </a>
              </div>
            </div>
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">

          <table class="table table-bordered table-striped">
            <tr>
              <td class="headings" width="200px">ID Klien</td>
              <td>{{$patient->id}}</td>
            </tr>
            <tr>
              <td class="headings" width="200px">ID Medis</td>
              <td>{{$patient->patient_medical_id}}</td>
            </tr>
            <tr>
              <td class="headings">Nama Lengkap</td>
              <td>{{$patient->patient_fullname}}</td>
            </tr>
            <tr>
              <td class="headings">Nama Panggilan</td>
              <td>{{$patient->patient_nickname}}</td>
            </tr>
            <tr>
              <td class="headings">Jenis Kelamin</td>
              <td>{{$patient->patient_gender}}</td>
            </tr>
            <tr>
              <td class="headings">Tempat Lahir</td>
              @if(is_null($patient->patient_regency_id))
              <td></td>
              @else
              <td>{{$patient->getRegency->regency_name}}</td>
              @endif
            </tr>
            <tr>
              <td class="headings">Tanggal Lahir</td>
              <td>{{$patient->patient_birth_date}}</td>
            </tr>
            <tr>
              <td class="headings">Pendidikan Terakhir</td>
              @if(is_null($patient->patient_last_education_id))
              <td></td>
              @else
              <td>{{$patient->getLastEducation->last_education_name}}</td>
              @endif
            </tr>
            <tr>
              <td class="headings">Pekerjaan</td>
              <td>{{$patient->patient_job}}</td>
            </tr>
            <tr>
              <td class="headings">Nomor Telepon / HP</td>
              <td>{{$patient->patient_phone_number}}</td>
            </tr>
            <tr>
              <td class="headings">Nomor Telepon / HP (2)</td>
              <td>{{$patient->patient_phone_number_2}}</td>
            </tr>
            <tr>
              <td class="headings">Email</td>
              <td>{{$patient->patient_email}}</td>
            </tr>
            <tr>
              <td class="headings">Alamat</td>
              <td>{{$patient->patient_address}}</td>
            </tr>
            <tr>
              <td class="headings">Agama</td>
              <td>{{$patient->patient_religion}}</td>
            </tr>
            <tr>
              <td class="headings">Status Perkawinan</td>
              <td>{{$patient->patient_mariage_status}}</td>
            </tr>
            <tr>
              <td class="headings">Info Mugi Barokah</td>
              <td>{{$patient->patient_source_info}}</td>
            </tr>
            <tr>
              <td class="headings">Riwayat Alergi</td>
              <td>{{$patient->patient_allergic_history}}</td>
            </tr>
            <tr>
              <td class="headings">Tanggal Masuk</td>
              <td>{{ date('d-m-Y', strtotime($patient->created_at)) }}</td>
            </tr>
          </table>

        </div>
      </div>
    </div>

  @stop