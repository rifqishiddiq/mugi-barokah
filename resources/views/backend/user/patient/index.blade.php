@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Tabel Klien</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active"><a href="#" style="color:#dee2e6">Klien</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Tabel Klien</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">{{$title}}</h3>
            </div>
            <div class="col-md-6">
              <div align="right">
              <?php $id = "add"?>
                <a href="{{ url('/patient/add',$id) }}">
                  <button align="right" type="button" class="btn bg-success text-white">  
                    <i class="glyphicon glyphicon-plus"></i>
                    Tambah Klien
                  </button>
                </a>
              </div>
            </div>
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">

          <table class="table table-striped" id="data-tables">
            <thead>
              <tr class="headings">
                <th width="30">No</th>
                <th style="text-align: center">Medical ID</th>
                <th style="text-align: center">Name</th>
                <th style="text-align: center">Phone Number</th>
                <th></th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>
  
    @stop

    @section('scripts')
    <script>

      function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apakah anda yakin akan menghapus data?", function () {
          ajaxTransfer("/patient/delete", data, "#modal-output");
        })
      }
      $(document).ready(function () {
        ajaxDataTable('#data-tables', '/patient/data-table', [
          {data: 'rownum', name: 'rownum'},
          {data: 'patient_medical_id', name: 'patient_medical_id', class:'text-center'},
          {data: 'patient_fullname', name: 'patient_fullname'},
          {data: 'patient_phone_number', name: 'patient_phone_number'},
          {data: 'action', name: 'action', orderable: false, searchable: false, class:'text-right'}
        ]);
      });

    </script>

  @stop