@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Tabel Klien</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active"><a href="#" style="color:#dee2e6">Klien ({{$patient->patient_fullname}})</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Tabel Rekam Medis</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">Tabel Rekam Medis ({{$patient->patient_fullname}})</h3>
            </div>
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">

          <table class="table table-striped" id="data-tables">
            <thead>
              <tr class="headings">
                <th style="max-width:30px">No</th>  
                <th style="text-align: center">Tanggal</th>
                <th style="text-align: center">Keluhan</th>
                <th style="text-align: center">Jenis Terapi</th>
                <th style="text-align: center">Terapist</th>
                <th></th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>

    @stop

    @section('scripts')
    <script>

      function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apakah anda yakin akan menghapus data?", function () {
          ajaxTransfer("/queue/delete", data, "#modal-output");
        })
      }

      $(document).ready(function() {
        var table = $('#data-tables').DataTable({
            "processing": true,
            "serverSide": true,

            ajax:{
              url: "{{url('medical-record/data-table')}}",
              data: {
                'patient_id': {{$patient->id}},
              },
            },
            columns: [
                {data: 'rownum', name: 'rownum'},
                {data: 'queue_date', name: 'queue_date', class:'text-center'},
                {data: 'therapy_recap_complaint', name: 'therapy_recap_complaint', class:'text-center'},
                {data: 'therapy_name', name: 'therapy_name', class:'text-center'},
                {data: 'employee_name', name: 'employee_name', class:'text-center'},
                {data: 'action', name: 'action', orderable: false, searchable: false, class:'text-right'},
            ],
            language:{paginate:{previous:"<i class='fas fa-angle-left'>",next:"<i class='fas fa-angle-right'>"}},
        });
      });

    </script>

  @stop