@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Tabel Pegawai</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active"><a href="#" style="color:#dee2e6">Pegawai</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Detail Pegawai</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">Detail Pegawai</h3>
            </div>
            <div class="col-md-6">
              <div align="right">
                <a href="{{ url('/employee/add', $employee->employee_user_id) }}">
                  <button align="right" type="button" class="btn bg-success text-white">  
                    <i class="glyphicon glyphicon-plus"></i>
                    Edit Pegawai
                  </button>
                </a>
              </div>
            </div>
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">

          <table class="table table-bordered table-striped">
            <tr>
              <td class="headings" width="200px">ID Pegawai</td>
              <td>{{$employee->id}}</td>
            </tr>
            <tr>
              <td class="headings">Nama Lengkap</td>
              <td>{{$employee->employee_name}}</td>
            </tr>
            <tr>
              <td class="headings">Posisi</td>
              <td>{{$employee->employee_position}}</td>
            </tr>
            <tr>
              <td class="headings">Jenis Kelamin</td>
              <td>{{$employee->employee_gender}}</td>
            </tr>
            <tr>
              <td class="headings">Tempat Lahir</td>
              <td>{{$employee->getRegency->regency_name}}</td>
            </tr>
            <tr>
              <td class="headings">Tanggal Lahir</td>
              <td>{{$employee->employee_birth_date}}</td>
            </tr>
            <tr>
              <td class="headings">Nomor Telepon / HP</td>
              <td>{{$employee->employee_phone_number}}</td>
            </tr>
            <tr>
              <td class="headings">Email</td>
              <td>{{$employee->employee_email}}</td>
            </tr>
            <tr>
              <td class="headings">Alamat</td>
              <td>{{$employee->employee_address}}</td>
            </tr>
          </table>

        </div>
      </div>
    </div>

  @stop