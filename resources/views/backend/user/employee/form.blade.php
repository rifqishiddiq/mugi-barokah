@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Tabel Pegawai</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active"><a href="#" style="color:#dee2e6">Pegawai</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Form Pegawai</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">Form Pegawai</h3>
            </div>
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">

        <form method="POST" action="{{ url('/employee/save') }}" class='form-horizontal'>
 
        @if( Session::get('activeUser')->user_role_id==0 )
            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Nama Lengkap</label>
                <div class='col-sm-7 col-xs-12'>
                    <input type="text" name="employee_name" class="form-control" value="{{$employee->employee_name}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Posisi</label>
                <div class='col-sm-7 col-xs-12'>
                    <input type="text" name="employee_position" class="form-control" value="{{$employee->employee_position}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Jenis Kelamin</label>
                <div class='col-sm-9 col-xs-12'>
                    @if(!is_null($employee->employee_gender))
                        @if($employee->employee_gender=='male')
                        <input type="radio" name="employee_gender" value="male" checked> Male <br>
                        <input type="radio" name="employee_gender" value="female" required> Female
                        @else
                        <input type="radio" name="employee_gender" value="male"> Male <br>
                        <input type="radio" name="employee_gender" value="female" required checked> Female
                        @endif
                    @else
                    <input type="radio" name="employee_gender" value="male"> Male <br>
                    <input type="radio" name="employee_gender" value="female" required> Female
                    @endif
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Tempat Lahir</label>
                <div class='col-sm-5 col-xs-12'>
                    @if(is_null($employee->employee_regency_id))
                    <input type="text" list="employee_regency_id" id="birthplace" name="birthplace" class="form-control" placeholder="choose your city birhtplace" value="" required>
                    @else
                    <input type="text" list="employee_regency_id" id="birthplace" name="birthplace" class="form-control" placeholder="choose your city birhtplace" value="{{$employee->getRegency->regency_name}}" required>
                    @endif
                    <datalist id="employee_regency_id" style="display:none">
                    <select>
                        @foreach($regency as $r)
                        <option data-value="{{$r->id}}">{{$r->regency_name}}</option>
                        @endforeach
                    </select>
                    </datalist>
                    <input type="hidden" name="employee_regency_id" id="birthplace-hidden">            
                </div>
            </div>

            <div class="form-group">
                <label for="deadline" class='col-sm-5 col-xs-12 control-label'>Tanggal Lahir</label>
                <div class='col-sm-3 col-xs-12'>
                <input type="date" name="employee_birth_date" class="form-control" value="{{$employee->employee_birth_date}}" max="{{date('dd/mm/yyyy')}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Nomor Telepon / HP</label>
                <div class='col-sm-5 col-xs-12'>
                    <input type="number" name="employee_phone_number" class="form-control" value="{{$employee->employee_phone_number}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Email</label>
                <div class='col-sm-9 col-xs-12'>
                    <input type="email" name="employee_email" class="form-control" value="{{$employee->employee_email}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Alamat</label>
                <div class='col-sm-9 col-xs-12'>
                    <textarea name="employee_address" class="form-control" value="{{$employee->employee_address}}" required>{{$employee->employee_address}}</textarea>
                </div>
            </div>

            <br>

            @else
            <input type="hidden" name="employee_name" class="form-control" value="{{$employee->employee_name}}">
            <input type="hidden" name="employee_position" class="form-control" value="{{$employee->employee_position}}">
            <input type="hidden" name="employee_gender" class="form-control" value="{{$employee->employee_gender}}">
            <input type="hidden" name="birthplace" class="form-control" value="{{$employee->getRegency->regency_name}}">
            <input type="hidden" name="employee_regency_id" class="form-control" value="{{$employee->employee_regency_id}}">            
            <input type="hidden" name="employee_birth_date" class="form-control" value="{{$employee->employee_birth_date}}">
            <input type="hidden" name="employee_email" class="form-control" value="{{$employee->employee_email}}">
            <input type="hidden" name="employee_address" class="form-control" value="{{$employee->employee_address}}">
            <input type="hidden" name="employee_phone_number" class="form-control" value="{{$employee->employee_phone_number}}">

            @endif
            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Username</label>
                <div class='col-sm-9 col-xs-12'>
                    <input type="text" name="user_username" class="form-control" value="{{$user->user_username}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label for='nama' class='col-sm-5 col-xs-12 control-label'>Password</label>
                <div class='col-sm-9 col-xs-12'>
                    <input type="password" name="user_password" class="form-control" value="{{$user->user_password}}" required>
                </div>
            </div>

            <div class='form-group'>
                <label class='col-sm-5 col-xs-12 control-label'></label>
                <div class='col-sm-9 col-xs-12'>
                    <input type="submit" class="btn btn-success" value="Add Patient">
                </div>
            </div>

            <input type='hidden' name='user_id' value='{{  encrypt($user->id) }}'>
            <input type='hidden' name='user_role_id' value='1'>
            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
            </form>



        </div>
      </div>
    </div>

  @stop

  @section('scripts')

  <script>
    $(document).ready( function($) {
        // Disable scroll when focused on a number input.
        $('form').on('focus', 'input[type=number]', function(e) {
            $(this).on('wheel', function(e) {
                e.preventDefault();
            });
        });
        // Restore scroll on number inputs.
        $('form').on('blur', 'input[type=number]', function(e) {
            $(this).off('wheel');
        });
        // Disable up and down keys.
        $('form').on('keydown', 'input[type=number]', function(e) {
            if ( e.which == 38 || e.which == 40 )
                e.preventDefault();
        });      

        $('form').on('focus', 'input[type=number]', function(e) {
            // Remove invalid characters
            var sanitized = $(this).val().replace(/[^-0-9]/g, '');
            // Remove non-leading minus signs
            sanitized = sanitized.replace(/(.)-+/g, '$1');
            // Update value
            $(this).val(sanitized);
        });
    });

    document.querySelector("input[list='employee_regency_id']").addEventListener('input', function(e) {
    var input = e.target,
        list = input.getAttribute('list'),
        options = document.querySelectorAll('#' + list + ' select option'),
        hiddenInput = document.getElementById(input.id + '-hidden'),
        inputValue = input.value;

    hiddenInput.value = options.value;

    for(var i = 0; i < options.length; i++) {
        var option = options[i];

        if(option.innerText === inputValue) {
            hiddenInput.value = option.getAttribute('data-value');
            break;
        }
    }
    });

  </script>
  @stop