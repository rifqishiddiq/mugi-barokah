@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
<style>

.center {
  position: relative;
  left: 50%;
  max-width: 900px;
  text-align: center;
}
</style>

    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 d-inline-block mb-0 text-white">Grafik Bulanan</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active"><a href="#" style="color:#dee2e6">Grafik Bulanan</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">{{date('F Y', strtotime($now)) }}</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-5 col-md-6">
            <div class="card bg-white border-0">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <h5 class="card-title text-uppercase text-muted mb-0 text-black">Bulan</h5>
                            <span class="h2 font-weight-bold mb-0 text-black">{{$now}}</span>
                        </div>
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0 text-black">Pilih Bulan</h5>
                            <form action="{{ url('chart/date') }}" method="POST">
                                @csrf
                                <select id="getMonth" name="month" required  style="color:#141414">
                                    <option value="{{$now}}" style="color:#8898aa">{{$now}}</option>
                                    @foreach ($queue as $q)
                                        <option value="{{$q->month}}" style="color:#141414">{{$q->month}}</option>
                                    @endforeach
                                </select>
                                <input type="submit" value="Choose" class="btn btn-primary" style="display:inline; padding:3px 5px">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12">

          <div class="card">
            <!-- Card body -->
            <div class="card-body">
                <!-- Chart wrapper -->
                <div id="gender_chart" class="center"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-12">

          <div class="card">
            <!-- Card body -->
            <div class="card-body">
                <!-- Chart wrapper -->
                <div id="therapy_chart" class="center"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-12">

          <div class="card">
            <!-- Card body -->
            <div class="card-body">
                <!-- Chart wrapper -->
                <div id="last_education_chart" class="center"></div>
            </div>
          </div>
        </div>
      </div>

</div>

@stop

@section('scripts')
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>
    $(function(){
        // Create gender pie chart
        Highcharts.chart('gender_chart', {
            chart: {
                type: 'pie',
                plotShadow: false,
            },
            credits: {
                enabled: false
            }, 
            title: {
                text: 'JUMLAH KLIEN'
            },
            subtitle: {
                text: '<?php echo $now ?>'
            },
            xAxis: {
                categories: ['JUMLAH KLIEN'],
                labels: {
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            tooltip: {
                pointFormat: '<span style="color:{point.color}">{series.name}</span>:<b>{point.y}</b><br/>'
            },
            legend: {
                enabled: true
            },
            plotOptions: {
                pie: {
                    allowPointSelect: false,
                    // cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name} - </b>{point.y}'
                    },
                    showInLegend: true
                }
            },
            // series: [{
            //     name: 'Laki-laki',
            //     data: [<?php echo $chart_gender['laki_laki'] ?>]
            // }, {
            //     name: 'Perempuan',
            //     data: [<?php echo $chart_gender['perempuan'] ?>]
            // }]
            series: [{
                'name':'JUMLAH KLIEN',
                'data':[
                    ['Laki-laki', <?php echo $chart_gender['laki_laki'] ?>],
                    ['Perempuan', <?php echo $chart_gender['perempuan'] ?>],
                ]
            }]
        });

        // Create Therapy column chart
        Highcharts.chart('therapy_chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Terapi'
            },
            xAxis: {
                categories: ['Jenis Terapi'],
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah'
                }
            },
            legend: {
                enabled: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    },
                }
            },
            tooltip: {
                pointFormat: '<span style="color:{point.color}">{series.name}</span>: <b>{point.y}</b><br/>'
            },
            series: [{
                name: 'Bekam Sunnah',
                data: [<?php echo $chart_therapy['bekam'] ?>]
            }, {
                name: 'Topung',
                data: [<?php echo $chart_therapy['topung'] ?>]
            }, {
                name: 'Matalogi',
                data: [<?php echo $chart_therapy['matalogi'] ?>]
            }, {
                name: 'lain-lain',
                data: [<?php echo $chart_therapy['lain_lain'] ?>]
            }]
        });

        // Create LastEducation column chart
        Highcharts.chart('last_education_chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Pendidikan Terakhir Klien'
            },
            xAxis: {
                categories: ['Pendidikan Terakhir'],
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah'
                }
            },
            legend: {
                enabled: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    },
                }
            },
            tooltip: {
                pointFormat: '<span style="color:{point.color}">{series.name}</span>: <b>{point.y}</b><br/>'
            },
            series: [{
                name: '-',
                data: [<?php echo $chart_last_education[0] ?>]
            }, {
                name: 'SD',
                data: [<?php echo $chart_last_education[1] ?>]
            }, {
                name: 'SMP',
                data: [<?php echo $chart_last_education[2] ?>]
            }, {
                name: 'SMA/SMK/MA',
                data: [<?php echo $chart_last_education[3] ?>]
            }, {
                name: 'D1',
                data: [<?php echo $chart_last_education[4] ?>]
            }, {
                name: 'D2',
                data: [<?php echo $chart_last_education[5] ?>]
            }, {
                name: 'D3',
                data: [<?php echo $chart_last_education[6] ?>]
            }, {
                name: 'D4/S1',
                data: [<?php echo $chart_last_education[7] ?>]
            }, {
                name: 'S2',
                data: [<?php echo $chart_last_education[8] ?>]
            }, {
                name: 'S3',
                data: [<?php echo $chart_last_education[9] ?>]
            }]
        });

    })

</script>
@endsection
