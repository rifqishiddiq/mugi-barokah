@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 d-inline-block mb-0 text-white">Laporan Harian</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Laporan Harian</li>
                  <li class="text-right">
                  </li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
                <div align="right">
                    <a href="{{url('laporan_bulanan-excel', date('Y-m', strtotime($now)) )}}" target="_blank" title="Download Invoice">
                    <button align="right" type="button" class="btn btn-white">  
                        Laporan Bulanan
                    </button>
                    </a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card bg-graiyellow border-0">
                <!-- Card body -->
                <div class="row">
                    <div class="col-md-9" style="padding-left:22px; padding-top:10px">
                        <span class="h2 font-weight-bold mb-0 text-black;">{{date('d F Y', strtotime($now)) }}</span>
                    </div>
                    <div class="col">
                        <form action="{{ url('daily-report-daily') }}" method="POST" style="padding: 3px">
                            @csrf
                            <div style="padding-top:8px; padding-left:4px">
                                <input type="date" name="day" value="{{$now}}" style="border-style:solid; border-width:1px; border-color:rgb(169,169,169);">
                            </div>
                            <div style="padding:6px 0; padding-left:4px">
                                <input type="submit" value="Pilih" class="btn btn-primary" style="padding:3px 5px;">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-gradient-info border-0">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0 text-white">Klien</h5>
                            <?php $sum=0 ?>

                            <span class="h2 font-weight-bold mb-0 text-white">{{$queue->count()}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-gradient-success border-0">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0 text-white">Total Pemasukan</h5>

                            <?php $sum=0 ?>

                            @foreach($therapy_recap as $t)
                            @if(!is_null($t->therapy_recap_total_price))
                            <?php $sum=$sum+$t->therapy_recap_total_price ?>                            
                            @endif
                            @endforeach

                            <span class="h2 font-weight-bold mb-0 text-white">Rp {{ number_format($sum,2,',','.') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-gradient-danger border-0">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0 text-white">Subsidi</h5>

                            <?php $sum=0 ?>

                            @foreach($therapy_recap as $t)
                            @if(!is_null($t->therapy_recap_subsidy))
                            <?php $sum=$sum+$t->therapy_recap_subsidy ?>                            
                            @endif
                            @endforeach

                            <span class="h2 font-weight-bold mb-0 text-white">Rp {{ number_format($sum,2,',','.') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>

</div>

<div class="container-fluid mb-4">
    <div class="row card-wrapper">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title mb-3">Data Bulan: <span style="color:blue">{{date('F Y', strtotime($now)) }}</span></h3>

                    <table class="table table-bordered table-striped">
                        <tr>
                            <td class="headings" width="100px">Jumlah Klien</td>
                            <td>{{$queue_month->count()}}</td>
                        </tr>
                        <tr>
                            <td class="headings">Total Pemasukan</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_month as $t)
                                @if(!is_null($t->therapy_recap_total_price))
                                <?php $sum=$sum+$t->therapy_recap_total_price ?>                            
                                @endif
                            @endforeach

                            <td>Rp {{ number_format($sum,2,',','.') }}</td>
                        </tr>
                        <tr>
                            <td class="headings">Total Subsidi</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_month as $t)
                                @if(!is_null($t->therapy_recap_subsidy))
                                <?php $sum=$sum+$t->therapy_recap_subsidy ?>                            
                                @endif
                            @endforeach

                            <td>Rp {{ number_format($sum,2,',','.') }}</td>
                        </tr>
                        <tr>
                            <td class="headings">Bekam Sunnah</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_month as $t)
                                @if($t->therapy_recap_therapy_id == 1)
                                <?php $sum=$sum+1 ?>                            
                                @endif
                            @endforeach

                            <td>{{$sum}}</td>
                        </tr>
                        <tr>
                            <td class="headings">Topung</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_month as $t)
                                @if($t->therapy_recap_therapy_id == 2)
                                <?php $sum=$sum+1 ?>                            
                                @endif
                            @endforeach

                            <td>{{$sum}}</td>
                        </tr>
                        <tr>
                            <td class="headings">Matalogi</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_month as $t)
                                @if($t->therapy_recap_therapy_id == 3)
                                <?php $sum=$sum+1 ?>                            
                                @endif
                            @endforeach

                            <td>{{$sum}}</td>
                        </tr>
                        <tr>
                            <td class="headings">Lain-lain</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_year as $t)
                                @if($t->therapy_recap_therapy_id == 4)
                                <?php $sum=$sum+1 ?>                            
                                @endif
                            @endforeach

                            <td>{{$sum}}</td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
            <div class="card-body">
                    <h3 class="card-title mb-3">Data Tahun: <span style="color:blue">{{date('Y', strtotime($now)) }}</span></h3>

                    <table class="table table-bordered table-striped">
                        <tr>
                            <td class="headings" width="100px">Jumlah Klien</td>
                            <td>{{$queue_year->count()}}</td>
                        </tr>
                        <tr>
                            <td class="headings">Total Pemasukan</td>
                            <?php $sum=0 ?>
                            @foreach($therapy_recap_year as $t)
                                @if(!is_null($t->therapy_recap_total_price))
                                <?php $sum=$sum+$t->therapy_recap_total_price ?>                            
                                @endif
                            @endforeach

                            <td>Rp {{ number_format($sum,2,',','.') }}</td>
                        </tr>
                        <tr>
                            <td class="headings">Total Subsidi</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_year as $t)
                                @if(!is_null($t->therapy_recap_subsidy))
                                <?php $sum=$sum+$t->therapy_recap_subsidy ?>                            
                                @endif
                            @endforeach

                            <td>Rp {{ number_format($sum,2,',','.') }}</td>
                        </tr>
                        <tr>
                            <td class="headings">Bekam Sunnah</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_year as $t)
                                @if($t->therapy_recap_therapy_id == 1)
                                <?php $sum=$sum+1 ?>                            
                                @endif
                            @endforeach

                            <td>{{$sum}}</td>
                        </tr>
                        <tr>
                            <td class="headings">Topung</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_year as $t)
                                @if($t->therapy_recap_therapy_id == 2)
                                <?php $sum=$sum+1 ?>                            
                                @endif
                            @endforeach

                            <td>{{$sum}}</td>
                        </tr>
                        <tr>
                            <td class="headings">Matalogi</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_year as $t)
                                @if($t->therapy_recap_therapy_id == 3)
                                <?php $sum=$sum+1 ?>                            
                                @endif
                            @endforeach

                            <td>{{$sum}}</td>
                        </tr>
                        <tr>
                            <td class="headings">Lain-lain</td>

                            <?php $sum=0 ?>
                            @foreach($therapy_recap_year as $t)
                                @if($t->therapy_recap_therapy_id == 4)
                                <?php $sum=$sum+1 ?>                            
                                @endif
                            @endforeach

                            <td>{{$sum}}</td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
        <div>
    </div>
</div>

    <!-- <div class="container-fluid mt--6">
      <div class="card mb-4">

        <div class="card-body">

        </div>
      </div>
    </div> -->

@stop

