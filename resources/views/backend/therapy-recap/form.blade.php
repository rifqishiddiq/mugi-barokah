@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
<style>
.form-control {
    color:#555555;
}
</style>
<div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-9 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Medical Record</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active"><a href="#" class="text-white">Home</a></li>
                  <li class="breadcrumb-item"><a href="#" class=" text-white">Medical Record</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Form</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">Medical Record Form</h3>
            </div>
          </div>

        </div>
        <div class="card-body">
                  
                  <form method="POST" action="{{ url('/medical-record/save') }}" class='form-horizontal'>
                    
                    <div class='form-group'>
                        <label for='nama' class='col-sm-5 col-xs-12 control-label'>ID Antrian</label>
                        <div class='col-sm-1 col-xs-12'>
                            <label for='nama' class='control-label'>{{$queue->id}}
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='nama' class='col-sm-5 col-xs-12 control-label'>Keluhan</label>
                        <div class='col-sm-6 col-xs-12'>
                            @if(is_null($therapy_recap->therapy_recap_complaint))
                            <textarea type="textarea" name="therapy_recap_complaint" class="form-control" value="{{$therapy_recap->therapy_recap_complaint}}"></textarea>
                            @else
                            <textarea type="textarea" name="therapy_recap_complaint" class="form-control" value="{{$therapy_recap->therapy_recap_complaint}}">{{$therapy_recap->therapy_recap_complaint}}</textarea>
                            @endif
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='nama' class='col-sm-5 col-xs-12 control-label'>Jenis Terapi</label>
                        <div class='col-md-3'>
                            <select class="form-control" id='therapy' name='therapy_recap_therapy_id' required="required">
                            @if(is_null($therapy_recap->id))
                            <option value=''></option>
                                @foreach($therapy as $t)
                                    <option value='{{$t->id}}'>{{$t->therapy_name}}</option>
                                @endforeach
                            @else
                            <option value='{{$therapy_recap->getTherapy->id}}'>{{$therapy_recap->getTherapy->therapy_name}}</option>
                                @foreach($therapy as $t)
                                @if($t->id != $therapy_recap->getTherapy->id)
                                    <option value='{{$t->id}}'>{{$t->therapy_name}}</option>
                                @endif
                                @endforeach
                            @endif
                            </select>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='nama' class='col-sm-5 col-xs-12 control-label'>Nama Terapist</label>
                        <div class='col-md-4'>
                            <select class="form-control" id='therapist' name='therapy_recap_therapist_id' required="required">
                            @if(is_null($therapy_recap->id))
                            <option value=''></option>
                                @foreach($therapist as $t)
                                    <option value='{{$t->id}}'>{{$t->employee_name}}</option>
                                @endforeach
                            @else
                            <option value='{{$therapy_recap->getTherapist->id}}'>{{$therapy_recap->getTherapist->employee_name}}</option>
                                @foreach($therapist as $t)
                                @if($t->id != $therapy_recap->getTherapist->id)
                                    <option value='{{$t->id}}'>{{$t->employee_name}}</option>
                                @endif
                                @endforeach
                            @endif
                            </select>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='nama' class='col-sm-5 col-xs-12 control-label'>Hasil Pemeriksaan</label>
                        <div class='col-sm-6 col-xs-12'>
                            @if(is_null($therapy_recap->therapy_recap_result))
                            <textarea type="textarea" name="therapy_recap_result" class="form-control" value="{{$therapy_recap->therapy_recap_result}}"></textarea>
                            @else
                            <textarea type="textarea" name="therapy_recap_result" class="form-control" value="{{$therapy_recap->therapy_recap_result}}">{{$therapy_recap->therapy_recap_result}}</textarea>
                            @endif
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='nama' class='col-sm-5 col-xs-12 control-label'>Resep</label>
                        <div class='col-sm-6 col-xs-12'>
                            @if(is_null($therapy_recap->therapy_recap_receipt))
                            <textarea type="textarea" name="therapy_recap_receipt" class="form-control" value="{{$therapy_recap->therapy_recap_receipt}}"></textarea>
                            @else
                            <textarea type="textarea" name="therapy_recap_receipt" class="form-control" value="{{$therapy_recap->therapy_recap_receipt}}">{{$therapy_recap->therapy_recap_receipt}}</textarea>
                            @endif
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='nama' class='col-sm-5 col-xs-12 control-label'>Harga</label>
                        <div class='col-sm-2 col-xs-12'>
                            <input type="number" name="therapy_recap_price" class="form-control" value="{{$therapy_recap->therapy_recap_price}}" required>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label for='nama' class='col-sm-5 col-xs-12 control-label'>Subsidi</label>
                        <div class='col-sm-2 col-xs-12'>
                            <input type="number" name="therapy_recap_subsidy" class="form-control" value="{{$therapy_recap->therapy_recap_subsidy}}">
                        </div>
                    </div>

                    <div class='form-group'>
                        <label class='col-sm-2 col-xs-12 control-label'></label>
                        <div class='col-sm-9 col-xs-12'>
                                <input type="submit" class="btn btn-success" value="Add Data">
                            <a>
                        </div>
                    </div>

                    <input type='hidden' name='therapy_recap_id' value='{{ encrypt($therapy_recap->id) }}'>
                    <!-- <input type='hidden' name='therapy_recap_queue_id' value='{{ $queue->id }}'> -->
                    <input type='hidden' name='queue_id' value='{{ $queue->id }}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                </form> 

        </div>
      </div>
    </div>

@stop

@section('scripts')
<script>
// $(document).ready(function () {
//     $('#form-konten').submit(function () {
//         var data = getFormData('form-konten');
//         ajaxTransfer("/medical-record/save", data, '#result-form-konten');
//     })
// })

$(document).ready( function($) {

 // Disable scroll when focused on a number input.
    $('form').on('focus', 'input[type=number]', function(e) {
        $(this).on('wheel', function(e) {
            e.preventDefault();
        });
    });
    // Restore scroll on number inputs.
    $('form').on('blur', 'input[type=number]', function(e) {
        $(this).off('wheel');
    });
    // Disable up and down keys.
    $('form').on('keydown', 'input[type=number]', function(e) {
        if ( e.which == 38 || e.which == 40 )
            e.preventDefault();
    });      

    $('form').on('focus', 'input[type=number]', function(e) {
        // Remove invalid characters
        var sanitized = $(this).val().replace(/[^-0-9]/g, '');
        // Remove non-leading minus signs
        sanitized = sanitized.replace(/(.)-+/g, '$1');
        // Update value
        $(this).val(sanitized);
    });

});

@stop

