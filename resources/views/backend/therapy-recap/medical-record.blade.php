@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')

<style>
    .headings {
        text-align: right;
    }
</style>

    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-9 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Rekam Medis</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item"><a href="#" style="color:#dee2e6">Rekam Medis</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Detail</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">{{$title}}</h3>
            </div>
            <div class="col-md-6">
              <div align="right">
                @if( Session::get('activeUser')->user_role_id==2)
                <a href="{{url('rekam_medis-pdf', encrypt($therapy_recap->id) )}}" target="_blank" title="Download Invoice">
                  <button align="right" type="button" class="btn btn-primary">  
                    Download Resi
                  </button>
                </a>
                @else
                  <a href="{{ url('medical-record/add', $therapy_recap->id ) }}" title="Edit Medical Record">
                    <button align="right" type="button" class="btn btn-success">  
                      Edit Rekam Medis
                    </button>
                  </a>
                  <a href="{{url('rekam_medis-pdf', encrypt($therapy_recap->id) )}}" target="_blank" title="Download Invoice">
                    <button align="right" type="button" class="btn btn-primary">  
                      Download Resi
                    </button>
                  </a>
                @endif
              </div>
            </div>
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">
          @if( Session::get('activeUser')->user_role_id==2 )

          <table class="table table-bordered table-striped">
            <tr>
              <td class="headings" width="200px">ID Rsei</td>
              <td>{{$therapy_recap->id}}</td>
            </tr>
            <tr>
            <td class="headings">Nama Klien</td>
              <td>{{$therapy_recap->getQueue->getPatient->patient_fullname}}</td>
            </tr>
            <tr>
            <td class="headings">ID Rekam Klien</td>
              <td>{{$therapy_recap->getQueue->getPatient->patient_medical_id}}</td>
            </tr>
            <tr>
            <td class="headings">No HP</td>
              <td>{{$therapy_recap->getQueue->getPatient->patient_phone_number}}</td>
            </tr>
            <tr>
              <td class="headings">Keluhan</td>
              <td>{{$therapy_recap->therapy_recap_complaint}}</td>
            </tr>
            <tr>
              <td class="headings">Jenis Terapi</td>
              <td>{{$therapy_recap->getTherapy->therapy_name}}</td>
            </tr>
            <tr>
              <td class="headings">Therapist</td>
              <td>{{$therapy_recap->getTherapist->employee_name}}</td>
            </tr>
            <tr>
              <td class="headings">Resep</td>
              <td>{{$therapy_recap->therapy_recap_receipt}}</td>
            </tr>
            <tr>
              <td class="headings">Hasil Pengobatan</td>
              <td>{{$therapy_recap->therapy_recap_result}}</td>
            </tr>
            <tr>
              <td class="headings">Harga Terapi</td>
              <td>Rp {{ number_format($therapy_recap->therapy_recap_price,2,',','.') }}</td>
            </tr>
            <tr>
              <td class="headings">Total Harga</td>
              <td>Rp {{ number_format($therapy_recap->therapy_recap_total_price,2,',','.') }} <span style="color:gray; padding-left:20px">(harga yg tertera hanya biaya terapi, belum termasuk pembelian obat dan lain-lain) </span></td>
            </tr>
            <tr>
              <td class="headings">Subsidi</td>
              <td>Rp {{ number_format($therapy_recap->therapy_recap_subsidy,2,',','.') }}</td>
            </tr>
          </table>

          @else

          <table class="table table-bordered table-striped">
            <tr>
              <td class="headings" width="200px">ID Resi</td>
              <td>{{$therapy_recap->id}}</td>
            </tr>
            <tr>
            <td class="headings">Nama Klien</td>
              <td>{{$therapy_recap->getQueue->getPatient->patient_fullname}}</td>
            </tr>
            <tr>
            <td class="headings">ID Rekam Klien</td>
              <td>{{$therapy_recap->getQueue->getPatient->patient_medical_id}}</td>
            </tr>
            <tr>
              <td class="headings">Keluhan</td>
              <td>{{$therapy_recap->therapy_recap_complaint}}</td>
            </tr>
            <tr>
              <td class="headings">Jenis Terapi</td>
              <td>{{$therapy_recap->getTherapy->therapy_name}}</td>
            </tr>
              <td class="headings">Therapist</td>
              <td>{{$therapy_recap->getTherapist->employee_name}}</td>
            </tr>
            <tr>
              <td class="headings">Resep</td>
              <td>{{$therapy_recap->therapy_recap_receipt}}</td>
            </tr>
            <tr>
            <tr>
              <td class="headings">Hasil Pengobatan</td>
              <td>{{$therapy_recap->therapy_recap_result}}</td>
            </tr>
            <tr>
              <td class="headings">Harga Terapi</td>
              <td>Rp {{ number_format($therapy_recap->therapy_recap_price,2,',','.') }}</td>
            </tr>
            <tr>
              <td class="headings">Subsidi</td>
              <td>Rp {{ number_format($therapy_recap->therapy_recap_subsidy,2,',','.') }}</td>
            </tr>
            <tr>
              <td class="headings">Total Harga</td>
              <td>Rp {{ number_format($therapy_recap->therapy_recap_total_price,2,',','.') }} <span style="color:gray; padding-left:20px">(harga yg tertera hanya biaya terapi, belum termasuk pembelian obat dan lain-lain) </span></td>
            </tr>
          </table>

          @endif

        </div>
      </div>
    </div>

    @stop
