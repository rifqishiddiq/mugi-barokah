@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')
    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Tabel Rekam Medis</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item active"><a href="#" style="color:#dee2e6">Rekam Medis</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Tabel Rekam Medis</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">{{$title}}</h3>
            </div>
            <!-- <div class="col-md-6">
              <div align="right">
                <a onclick="loadModal(this)" target="/user/add" data="role=2" title="Add Patient">
                  <button align="right" type="button" class="btn bg-success text-white">  
                    <i class="glyphicon glyphicon-plus"></i>
                    Add Patient
                  </button>
                </a>
              </div>
            </div> -->
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">

          <table class="table table-striped" id="data-tables">
            <thead>
              <tr class="headings">
                <th style="max-width:30px">No</th>  
                <th style="text-align: center">Tanggal</th>
                <th style="text-align: center">Jam</th>
                <th style="text-align: center">Nama Klien</th>
                <th style="text-align: center">Jenis Terapi</th>
                <th></th>
              </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <td style="padding-right:0px" colspan="2">
                        <input class="form-control filter-input" placeholder="input Tanggal..." type="text" data-column="1" style="width:150px">
                    </td>
                    <td>
                        <select data-column="4" class="form-control filter-select" colspan="3">
                            <option value="">pilih jenis terapi...</option>
                            @foreach($therapy as $t)
                                <option value="{{$t->therapy_name}}">{{$t->therapy_name}}</option>
                            @endforeach
                    </td>
                </tr>
            </tfoot>
          </table>

        </div>
      </div>
    </div>
  
    @stop

    @section('scripts')
    <script>

      $(document).ready(function() {
        var table = $('#data-tables').DataTable({
            "processing": true,
            "serverSide": true,

            ajax: "{{ url('/medical-record/data-table') }}",
            columns: [
                {data: 'rownum', name: 'rownum'},
                {data: 'queue_date', name: 'queue_date', class:'text-center'},
                {data: 'queue_time', name: 'queue_time', class:'text-center'},
                {data: 'patient_fullname', name: 'patient_fullname', class:'text-center'},
                {data: 'therapy_name', name: 'therapy_name', class:'text-center'},
                {data: 'action', name: 'action', orderable: false, searchable: false, class:'text-right'},
            ],
        });

        $('.filter-input').keyup(function() {
            table.column( $(this).data('column') )
            .search( $(this).val() )
            
            .draw();
        });
  
        $('.filter-select').change(function() {
            table.column( $(this).data('column') )
            .search( $(this).val() )
            .draw();
        });

      });

    </script>

  @stop