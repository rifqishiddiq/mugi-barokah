@extends('layouts.master')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Tables</a>
                </li>


                <li class="active">
                    <a>{{$title}}</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>{{$title}}</h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <div class="panel-body">
                      <table class="table table-striped" id="data-tables">
                      <thead>
                          <tr class="headings">
                            <th width="30">No</th>
                            <th style="text-align: center">Provice</th>
                            <th style="text-align: center">Regency Name</th>
                            <th></th>
                          </tr>
                        </thead>
                        <!-- <tfoot>
                          <tr class="footers">
                            <th width="30">No</th>
                            <th style="text-align: center">Username</th>
                            <th style="text-align: center">Role</th>
                            <th></th>
                          </tr>
                        </tfoot> -->
                      </table>
                    </div>
                  </div>
                </div>
              </div>

    @stop

    @section('scripts')
    <script>

      function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apakah anda yakin akan menghapus data?", function () {
          ajaxTransfer("/user/delete", data, "#modal-output");
        })
      }
      $(document).ready(function () {
        ajaxDataTable('#data-tables', '/regency/data-table', [
          {data: 'rownum', name: 'rownum'},
          {data: 'province_name', name: 'province_name'},
          {data: 'regency_name', name: 'regency_name'},
          {data: 'action', name: 'action', orderable: false, searchable: false, class:'text-right'}
        ]);
      });

        // function deleteData(id) {
        //     var data = new FormData();
        //     data.append('id', id);

        //     modalConfirm("Konfirmasi", "Apakah anda yakin akan menghapus data?", function () {
        //         ajaxTransfer("user/delete", data, "#modal-output");
        //     })
        // }
    </script>

  @stop