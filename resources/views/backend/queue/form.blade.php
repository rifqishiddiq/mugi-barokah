<style>
    .modal-body {
        max-height: calc(110vh - 210px);
        overflow-y: auto;
    }
</style>

<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
 
    @if(Session::get('activeUser')->user_role_id!=2)
    @if(!is_null($queue->queue_patient_id))
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Nama Klien</label>
        <div class='col-sm-5 col-xs-12'>
            <input type="text" name="patient" value="{{$queue->getPatient->patient_fullname}}" class="form-control" readonly style="background-color:#ffffff">
        </div>
        <input type="hidden" name="patient_id" value="{{$queue->queue_patient_id}}">            
    </div>
    @else
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Nama Klien</label>
        <div class='col-sm-5 col-xs-12'>
            <input type="text" list="patient_id" id="patient" name="patient" class="form-control" placeholder="input nama pasien" required>
            <datalist id="patient_id" style="display:none">
            <select>
                @foreach($patient as $p)
                <option data-value="{{$p->id}}">{{$p->patient_fullname}}</option>
                @endforeach
            </select>
            </datalist>
            <input type="hidden" name="patient_id" id="patient-hidden">            
        </div>
    </div>
    @endif
    @endif


    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Tanggal</label>
        <div class='col-sm-3 col-xs-12'>
        @if( Session::get('activeUser')->user_role_id==2 )
            @if(!is_null($queue->queue_date))
            <input name="queue_date" type="date" min="{{$now}}" class="form-control" value="{{$queue->queue_date}}" required>
            @else
            <input name="queue_date" type="date" min="{{$now}}" class="form-control" value="{{$now}}" required>
            @endif
        @else
            @if(!is_null($queue->queue_date))
            <input name="queue_date" type="date" class="form-control" value="{{$queue->queue_date}}" required>
            @else
            <input name="queue_date" type="date" class="form-control" value="{{$now}}" required>
            @endif
        @endif
        </div>
    </div>


    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Jam</label>
        <div class='col-md-3'>
            <select class="form-control" id='time' name='queue_time' required="required">
                @if(!is_null($queue->queue_time))
                <option value="{{date('H:i', strtotime($queue->queue_time))}}">{{date('H:i', strtotime($queue->queue_time))}}</option>
                @foreach($time as $t)
                    <option value='{{$t}}'>{{$t}}</option>
                @endforeach
                @else
                @foreach($time as $t)
                    <option value='{{$t}}'>{{$t}}</option>
                @endforeach
                @endif
            </select>
        </div>
    </div>

    <div class='form-group'>
        <label for='nama' class='col-sm-5 col-xs-12 control-label'>Catatan</label>
        <div class='col-sm-9 col-xs-12'>
            @if(!is_null($queue->queue_note))
            <textarea name="queue_note" class="form-control" value="{{$queue->queue_note}}">{{$queue->queue_note}}</textarea>
            @else
            <textarea name="queue_note" class="form-control" value="{{$queue->queue_note}}"></textarea>
            @endif
        </div>
    </div>

    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
        <input type='hidden' name='queue_id' value="{{ encrypt($queue->id) }}">
        @if(!is_null($queue->id))
            <input type="submit" class="btn btn-primary" value="Edit Antrian">
        @else
            <input type="submit" class="btn btn-primary" value="Tambah Antrian">
        @endif
        </div>
    </div>
    <input type='hidden' name='user_role' value="{{Session::get('activeUser')->user_role_id}}">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

</div>

<script>

    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("/queue/save", data, '#result-form-konten');
        })
    })

    $(document).ready( function($) {

     // Disable scroll when focused on a number input.
        $('form').on('focus', 'input[type=number]', function(e) {
            $(this).on('wheel', function(e) {
                e.preventDefault();
            });
        });
        // Restore scroll on number inputs.
        $('form').on('blur', 'input[type=number]', function(e) {
            $(this).off('wheel');
        });
        // Disable up and down keys.
        $('form').on('keydown', 'input[type=number]', function(e) {
            if ( e.which == 38 || e.which == 40 )
                e.preventDefault();
        });      

        $('form').on('focus', 'input[type=number]', function(e) {
            // Remove invalid characters
            var sanitized = $(this).val().replace(/[^-0-9]/g, '');
            // Remove non-leading minus signs
            sanitized = sanitized.replace(/(.)-+/g, '$1');
            // Update value
            $(this).val(sanitized);
        });

    });

    document.querySelector('input[list]').addEventListener('input', function(e) {
        var input = e.target,
            list = input.getAttribute('list'),
            options = document.querySelectorAll('#' + list + ' select option'),
            hiddenInput = document.getElementById(input.id + '-hidden'),
            inputValue = input.value;

        hiddenInput.value = options.value;

        for(var i = 0; i < options.length; i++) {
            var option = options[i];

            if(option.innerText === inputValue) {
                hiddenInput.value = option.getAttribute('data-value');
                break;
            }
        }
    });

</script>
