@extends('layouts.argon-dashboard')
@section('title', $title)

@section('content')

<style>
    .headings {
        text-align: right;
    }
</style>

    <div class="header bg-gradient-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-9 col-7">
            <h6 class="h2 d-inline-block mb-0 text-white">Antrian</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home text-white"></i></a></li>
                  <li class="breadcrumb-item"><a href="#" style="color:#dee2e6">Antrian</a></li>
                  <li class="breadcrumb-item active text-white" aria-current="page">Detail</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <div id="hasil-output"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">

          <div class="row">
            <div class="col-md-6">
              <h3 class="mb-0">{{$title}}</h3>
            </div>
            <div class="col-md-6">
              <div align="right">
                @if( Session::get('activeUser')->user_role_id!=2)
                  <a onclick="loadModal(this)" target="/queue/add" data="queue_id='{{encrypt($queue_id)}}'" title="Edit Antrian">
                    <button align="right" type="button" class="btn btn-primary">  
                      Edit Antrian
                    </button>
                  </a>
                  <a href="{{ url('medical-record/add', encrypt($queue_id) ) }}" title="Add Medical Record">
                    <button align="right" type="button" class="btn btn-success">  
                      Tambah Rekam Medis
                    </button>
                  </a>
                @else
                @if($set == 1)
                  <a onclick="loadModal(this)" target="/queue/add" data="queue_id='{{encrypt($queue_id)}}'" title="Edit Antrian">
                    <button align="right" type="button" class="btn btn-primary">  
                      Edit Antrian
                    </button>
                  </a>
                @endif
                @endif
              </div>
            </div>
          </div>

        </div>
        <!-- Card body -->
        <div class="card-body">
          @if( Session::get('activeUser')->user_role_id!=2 )

          <table class="table table-bordered table-striped">
            <tr>
              <td class="headings" width="200px">Tanggal</td>
              <td>{{$queue->queue_date}}</td>
            </tr>
            <tr>
              <td class="headings">Jam</td>
              <td>{{$queue->queue_time}}</td>
            </tr>
            <tr>
              <td class="headings">ID Klien</td>
              <td>{{$queue->queue_patient_id}}</td>
            </tr>
            <tr>
              <td class="headings">ID Rekam Klien</td>
              <td>{{$queue->getPatient->patient_medical_id}}</td>
            </tr>
            <tr>
              <td class="headings">Nama Klien</td>
              <td>{{$queue->getPatient->patient_fullname}}</td>
            </tr>
            <tr>
              <td class="headings">No HP</td>
              <td>{{$queue->getPatient->patient_phone_number}}</td>
            </tr>
            <tr>
              <td class="headings">No HP 2</td>
              <td>{{$queue->getPatient->patient_phone_number_2}}</td>
            </tr>
            <tr>
              <td class="headings">Keterangan lain</td>
              <td>{{$queue->queue_note}}</td>
            </tr>
          </table>

          @else

          <table class="table table-bordered table-striped">
            <tr>
              <td class="headings" width="200px">Tanggal</td>
              <td>{{$queue->queue_date}}</td>
            </tr>
            <tr>
              <td class="headings">Jam</td>
              <td>{{$queue->queue_time}}</td>
            </tr>
            <tr>
              <td class="headings">Catatan</td>
              <td>{{$queue->queue_note}}</td>
            </tr>
          </table>

          @endif

        </div>
      </div>
    </div>

    @stop
