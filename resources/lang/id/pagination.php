<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 14/02/19
 * Time: 03.58
 */

return [
    'previous' => '&laquo; Sebelumnya',
    'next'     => 'Berikutnya &raquo;',
];