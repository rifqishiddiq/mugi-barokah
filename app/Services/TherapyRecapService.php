<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/02/19
 * Time: 21.12
 */

namespace App\Services;


use App\Core\Core;
use App\Models\TherapyRecap;
use Yajra\DataTables\DataTables;

class TherapyRecapService
{
    const MODEL_NAME=TherapyRecap::class;

    public function getData()
    {
        return Core::getData(self::MODEL_NAME);
    }

    public function findOne($id)
    {
        return Core::findOne($id,self::MODEL_NAME);
    }

    public function actionForm($id)
    {
        return Core::actionForm($id,self::MODEL_NAME);
    }

    public function actionSave($params)
    {
        // dd($params);
        return Core::actionSave($params,self::MODEL_NAME);
    }

    public function actionDelete($id)
    {
        return Core::actionDelete($id,self::MODEL_NAME);
    }

    public function actionSaveGetId($params)
    {
        return Core::actionSaveGetId($params,self::MODEL_NAME);
    }

    public function actionDataTable($params, $patientId)
    {
        $model=self::MODEL_NAME;
        $data=$model::dataTable($patientId);
        $dataTables = DataTables::of($data)
            ->addColumn('action', function ($list) {
                return "<a class='btn btn-sm btn-primary' href='".url('medical-record/detail', encrypt($list->therapy_recap_queue_id))."' title='Detail'>
                    <span class='btn-inner--text' style='text-align: center; padding: 0'>Detail</span></a>";
            })
            ->filter(function ($query) use ($params) {
                if ($params->input('search')['value']) {
                    $query->where('queue_date', 'like', "%{$params->input('search')['value']}%")
                    ->orWhere('queue_time', 'like', "%{$params->input('search')['value']}%")
                    ->orWhere('therapy_name', 'like', "%{$params->input('search')['value']}%");
                }
            })
            ->setRowId('id');

        return $dataTables->make(true);
    }

}