<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/02/19
 * Time: 21.12
 */

namespace App\Services;


use App\Core\Core;
use App\Models\Employee;
use DataTables;

class EmployeeService
{
    const MODEL_NAME=Employee::class;

    public function getData()
    {
        return Core::getData(self::MODEL_NAME);
    }

    public function findOne($id)
    {
        return Core::findOne($id,self::MODEL_NAME);
    }   

    public function actionForm($id)
    {
        return Core::actionForm($id,self::MODEL_NAME);
    }

    public function actionSave($params,$id)
    {
        // return Core::actionSave($params,self::MODEL_NAME);
        if(!empty($params)){
            $deleteCondition = ['employee_user_id' => $id];
            Core::actionDeleteWhere($deleteCondition, self::MODEL_NAME);
            $currentParams=[
                'employee_user_id' => $id,
            ];
            $currentParams = array_merge($currentParams, $params);
            // dd($params);
            return Core::actionInsertArrays($currentParams, self::MODEL_NAME);
        }

    }

    public function actionDelete($id)
    {
        return Core::actionDelete($id,self::MODEL_NAME);
    }

    public function actionDataTable($params)
    {
        $model=self::MODEL_NAME;
        $data=$model::dataTable();
        $dataTables = DataTables::of($data)
            ->addColumn('action', function ($list) {
                return "<a class='btn btn-sm btn-primary text-secondary' href='".url('/employee/detail', encrypt($list->id))."' title='Detail'>
                    <span class='btn-inner--text' style='text-align: center; padding: 0'>Detail</span></a>
                    <a class='btn btn-sm btn-danger text-secondary' onclick=deleteData('".encrypt($list->id)."') title='Delete'>
                    <span class='btn-inner--text' style='text-align: center; padding: 0'>Delete</span></a>";
            })
            // ->filter(function ($query) use ($params) {
            //     if ($params->input('search')['value']) {
            //         $query->where('username', 'like', "%{$params->input('search')['value']}%")
            //         ->orWHere('name', 'like', "%{$params->input('search')['value']}%");
            //     }
            // })
            ->setRowId('id');



        return $dataTables->make(true);
    }

}