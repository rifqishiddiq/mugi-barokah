<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/02/19
 * Time: 21.12
 */

namespace App\Services;


use App\Core\Core;
use App\Models\Regency;
use DataTables;

class RegencyService
{
    const MODEL_NAME=Regency::class;

    public function getData()
    {
        return Core::getData(self::MODEL_NAME);
    }

    public function findOne($id)
    {
        return Core::findOne($id,self::MODEL_NAME);
    }

    public function actionForm($id)
    {
        return Core::actionForm($id,self::MODEL_NAME);
    }

    public function actionSave($params)
    {
        return Core::actionSave($params,self::MODEL_NAME);
    }

    public function actionDelete($id)
    {
        return Core::actionDelete($id,self::MODEL_NAME);
    }

    public function actionSaveGetId($params)
    {
        return Core::actionSaveGetId($params,self::MODEL_NAME);
    }

    public function actionDataTable($params)
    {
        $model=self::MODEL_NAME;
        $data=$model::dataTable();
        $dataTables = DataTables::of($data)
            ->addColumn('action', function ($list) {
                return "<a class='btn btn-sm btn-primary' onclick='loadModal(this)' target='/user/add' data='id=".encrypt($list->id)."' title='Edit'>
                                                <i class='glyphicon glyphicon-edit'></i> Edit</a>
                                                <a class='btn btn-sm btn-danger' onclick=deleteData('".encrypt($list->id)."') title='Hapus'>
                                                <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->filter(function ($query) use ($params) {
                if ($params->input('search')['value']) {
                    $query->where('name', 'like', "%{$params->input('search')['value']}%")
                    ->orWhere('province_name', 'like', "%{$params->input('search')['value']}%");
                }
            })
            ->setRowId('id');



        return $dataTables->make(true);
    }

}