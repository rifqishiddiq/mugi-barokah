<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/02/19
 * Time: 21.12
 */

namespace App\Services;


use App\Core\Core;
use App\Models\Patient;
use DataTables;

class PatientService
{
    const MODEL_NAME=Patient::class;

    public function getData()
    {
        return Core::getData(self::MODEL_NAME);
    }

    public function findOne($id)
    {
        return Core::findOne($id,self::MODEL_NAME);
    }

    public function actionForm($id)
    {
        return Core::actionForm($id,self::MODEL_NAME);
    }

    public function actionSave($params,$id)
    {
        // return Core::actionSave($params,self::MODEL_NAME);
        if(!empty($params)){
            $deleteCondition = ['patient_user_id' => $id];
            Core::actionDeleteWhere($deleteCondition, self::MODEL_NAME);
            $currentParams=[
                'patient_user_id' => $id,
            ];
            $currentParams = array_merge($currentParams, $params);
            // dd($currentParams);
            return Core::actionInsertArrays($currentParams, self::MODEL_NAME);
        }
    }

    public function actionDelete($id)
    {
        return Core::actionDelete($id,self::MODEL_NAME);
    }

    public function actionDataTable($params)
    {
        $model=self::MODEL_NAME;
        $data=$model::dataTable();
        $dataTables = DataTables::of($data)
            ->addColumn('action', function ($list) {
                return "<a class='btn btn-sm btn-default text-secondary' href='".url('/patient/medical-record', $list->id)."' title='Rekam Medis'>
                    <span class='btn-inner--text' style='text-align: center; padding: 0'>Rekam Medis</span></a>
                    <a class='btn btn-sm btn-primary text-secondary' href='".url('/patient/detail', encrypt($list->id))."' title='Detail'>
                    <span class='btn-inner--text' style='text-align: center; padding: 0'>Detail</span></a>
                    <a class='btn btn-sm btn-danger text-secondary' onclick=deleteData('".encrypt($list->patient_user_id)."') title='Delete'>
                    <span class='btn-inner--text' style='text-align: center; padding: 0'>Delete</span></a>";
        })
            ->filter(function ($query) use ($params) {
                if ($params->input('search')['value']) {
                    $query->where('patient_fullname', 'like', "%{$params->input('search')['value']}%")
                    ->orWHere('patient_medical_id', 'like', "%{$params->input('search')['value']}%")
                    ->orWHere('patient_phone_number', 'like', "%{$params->input('search')['value']}%");
                }
            })
            ->setRowId('id');

        return $dataTables->make(true);
    }

}