<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/02/19
 * Time: 21.12
 */

namespace App\Services;


use App\Core\Core;
use App\Models\User;
use DataTables;

class UserService
{
    const MODEL_NAME=User::class;

    public function getData()
    {
        return Core::getData(self::MODEL_NAME);
    }

    public function findOne($id)
    {
        return Core::findOne($id,self::MODEL_NAME);
    }

    public function actionForm($id)
    {
        return Core::actionForm($id,self::MODEL_NAME);
    }

    public function actionSave($params)
    {
        // return Core::actionSave($params,self::MODEL_NAME);
        $checking=$this->findOne($params['id']);

        if(is_null($checking['data'])){
            return Core::actionSaveGetId($params,self::MODEL_NAME);
        }else{
            if(is_null($params['document_legal']) || $params['document_legal']==''){
                $params['document_legal']=$checking['data']->document_legal;
                return Core::actionSaveGetId($params,self::MODEL_NAME);
            }else{
                return Core::actionSaveGetId($params,self::MODEL_NAME);
            }
        }
    }

    public function actionDelete($id)
    {
        return Core::actionDelete($id,self::MODEL_NAME);
    }

    public function actionDataTable($params)
    {
        $model=self::MODEL_NAME;
        $data=$model::dataTable();
        $dataTables = DataTables::of($data)
            ->addColumn('action', function ($list) {
                return "<a href='".url('/user/detail', encrypt($list->id))."' class='btn btn-sm btn-success' title='View'>
                                                <i class='glyphicon glyphicon-eye-open'></i> View</a>
                                                <a class='btn btn-sm btn-primary' onclick='loadModal(this)' target='/user/add' data='id=".encrypt($list->id)."' title='Edit'>
                                                <i class='glyphicon glyphicon-edit'></i> Edit</a>
                                                <a class='btn btn-sm btn-danger' onclick=deleteData('".encrypt($list->id)."') title='Hapus'>
                                                <i class='glyphicon glyphicon-trash'></i> Delete</a>";
            })
            ->filter(function ($query) use ($params) {
                if ($params->input('search')['value']) {
                    $query->where('user_username', 'like', "%{$params->input('search')['value']}%")
                    ->orWHere('role_name', 'like', "%{$params->input('search')['value']}%");
                }
            })
            ->setRowId('id');



        return $dataTables->make(true);
    }

}