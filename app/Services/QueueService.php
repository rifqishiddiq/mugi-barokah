<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 23/02/19
 * Time: 21.12
 */

namespace App\Services;


use App\Core\Core;
use App\Models\Queue;
use Yajra\DataTables\DataTables;

class QueueService
{
    const MODEL_NAME=Queue::class;

    public function getData()
    {
        return Core::getData(self::MODEL_NAME);
    }

    public function findOne($id)
    {
        return Core::findOne($id,self::MODEL_NAME);
    }

    public function actionForm($id)
    {
        return Core::actionForm($id,self::MODEL_NAME);
    }

    public function actionSave($params)
    {
        // dd($params);
        return Core::actionSave($params,self::MODEL_NAME);
    }

    public function actionDelete($id)
    {
        return Core::actionDelete($id,self::MODEL_NAME);
    }

    public function actionSaveGetId($params)
    {
        return Core::actionSaveGetId($params,self::MODEL_NAME);
    }

    public function actionDataTable($params,$patientId)
    {
        $model=self::MODEL_NAME;
        $data=$model::dataTable($patientId);
        if(is_null($patientId)){
            $dataTables = DataTables::of($data)
                ->addColumn('action', function ($list) {
                    return "<a class='btn btn-sm btn-primary' href='".url('queue/detail', encrypt($list->id))."' title='Detail'>
                        <span class='btn-inner--text' style='text-align: center; padding: 0'>Detail</span></a>
                        <a class='btn btn-sm btn-danger text-secondary' onclick=deleteData('".encrypt($list->id)."') title='Delete'>
                        <span class='btn-inner--text' style='text-align: center; padding: 0'>Delete</span></a>";
                })
                ->filter(function ($query) use ($params) {
                    if ($params->input('search')['value']) {
                        $query->where('queue_date', 'like', "%{$params->input('search')['value']}%")
                        ->orWhere('queue_time', 'like', "%{$params->input('search')['value']}%");
                    }
                })
                ->setRowId('id');
        } else{
            $dataTables = DataTables::of($data)
                ->addColumn('action', function ($list) {
                    return "<a class='btn btn-sm btn-primary' href='".url('queue/detail', encrypt($list->id))."' title='Detail'>
                        <span class='btn-inner--text' style='text-align: center; padding: 0'>Detail</span></a>";
                })
                ->filter(function ($query) use ($params) {
                    if ($params->input('search')['value']) {
                        $query->where('queue_date', 'like', "%{$params->input('search')['value']}%")
                        ->orWhere('queue_time', 'like', "%{$params->input('search')['value']}%");
                    }
                })
                ->setRowId('id');
        }



        return $dataTables->make(true);
    }

}