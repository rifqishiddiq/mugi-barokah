<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
Use App\Models\User;
Use App\Models\Patient;
use App\Models\RegisterPatient;
use App\Models\Regency;
use App\Models\LastEducation;
use Session;

class LoginController extends Controller
{
    public function index(Request $request){
        if ($request->session()->exists('activeUser')) {
            if(Session::get('activeUser')->user_role_id==2){
                if(is_null(Patient::where('patient_user_id', Session::get('activeUser')->id)->first())){
                    return redirect('register?id='.Session::get('activeUser')->id.'');
                }else{
                    return redirect('schedule');
                }
            }
            return redirect('daily-report');
        }else {
            return view('login.index');
        }
    }

    public  function login(Request $request){
        $username=$request->input('username');
        $password=$request->input('password');
        $activeUser=User::where('user_username', $username)->first();

        if(is_null($activeUser)){
            echo "<div class='alert alert-danger'>username tidak terdaftar!!</div>";
            return view('login.index');
        }
        if (!Hash::check($password, $activeUser->user_password)) {
            echo "<div class='alert alert-danger'>username atau Password Salah!</div>";
            return view('login.index');
        }

        $request->session()->put('activeUser', $activeUser);
        if(Session::get('activeUser')->user_role_id==2){
            return redirect('schedule');
        }
        return redirect('daily-report');
    }

    public function Register(Request $request){
        $user = new User();
        $patient = new RegisterPatient();
        if(isset($request->id)){
            $user = User::find($request->id);
        }
        $params=[
            'user'=>$user,
            'patient'=>$patient,
        ];

        // return response()->json($params);
        return view('login.register',$params);
    }

    public function save(Request $request){
        // return response()->json($request->all());
        $user = User::find(decrypt($request->input('user_id')));        
        $new_password = $request->input('user_password'); 
        if(!isset($request->ada)){
            $user = new User();
            $user->user_username = $request->input('user_username');
            $user->user_password = Hash::make($request->input('user_password'));
            $user->user_role_id = $request->input('user_role_id');
            $user->save();
        }

        $patient = new RegisterPatient(); 
        $patient->patient_user_id = $user->id;
        $patient->patient_fullname = $request->input('patient_fullname');
        $patient->patient_gender = $request->input('patient_gender');
        $patient->patient_phone_number = $request->input('patient_phone_number');
        $patient->save();
        
        return redirect('/');
    }

    public function logout(Request $request)
    {
        Session::flush();
        return redirect('daily-report');
    }
}