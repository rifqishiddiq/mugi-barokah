<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use DateTime;
use App\Models\Queue;
use App\Models\Patient;
use App\Models\TherapyRecap;
use App\Models\LastEducation;
date_default_timezone_set('Asia/Jakarta');

class BackendController extends Controller
{

    private $titlePage='Dashboard';
    private $view='dashboard';

    public function index(Request $request){        
        $now = date('Y-m-d');
        $data_day = session()->get('data_day');
        if(!is_null($data_day)){
            $now = session()->get('data_day');
        }

        $now2 = new DateTime($now);
        $year = $now2->format('Y');
        $month = $now2->format('m');

        $queue = Queue::where('queue_date', $now)->where('queue_status_check',1)->get();

        $queue_id = Queue::select('id')->where('queue_date', $now)->where('queue_status_check',1)->get();
        $therapy_recap = TherapyRecap::whereIn('therapy_recap_queue_id', $queue_id)->get();

        $queue_therapy_month_id = Queue::select('id')->whereYear('queue_date', $year)->whereMonth('queue_date', $month)->where('queue_status_check', 1)->get();
        $queue_month = Queue::whereYear('queue_date', $year)->whereMonth('queue_date', $month)->where('queue_status_check',1)->get();
        $therapy_recap_month = TherapyRecap::whereIn('therapy_recap_queue_id', $queue_therapy_month_id)->get();

        $queue_therapy_year_id = Queue::select('id')->whereYear('queue_date', $year)->where('queue_status_check', 1)->get();
        $queue_year = Queue::whereYear('queue_date', $year)->where('queue_status_check',1)->get();
        $therapy_recap_year = TherapyRecap::whereIn('therapy_recap_queue_id', $queue_therapy_year_id)->get();

        $params = [
            'title' => "Laporan Harian",
            'now' => $now,
            'queue' => $queue,
            'therapy_recap' => $therapy_recap,
            'queue_month' => $queue_month,
            'therapy_recap_month' => $therapy_recap_month,
            'queue_year' => $queue_year,
            'therapy_recap_year' => $therapy_recap_year,
        ];

        return view('backend.daily-report.index', $params);
    }

    public function index2(Request $request){
        return redirect('daily-report')->with(['data_day' => $request->day]);
    }


    public function schedule(Request $request){        
        $now = date('Y-m-d');

        $today_queue = Queue::where('queue_date', $now)->get();
        $today_male = Queue::join('patients', 'patients.id', '=', 'queues.queue_patient_id')
            ->where('queue_date', $now)->where('patients.patient_gender', 'laki_laki')->get();
        $today_female = Queue::join('patients', 'patients.id', '=', 'queues.queue_patient_id')
            ->where('queue_date', $now)->where('patients.patient_gender', 'perempuan')->get();
        $patient_data = Patient::where('patient_user_id', Session::get('activeUser')->id)->first();

        $times_auto = $this->create_time_range('08:00', '15:30', '30 mins');
        $times_manual = $this->create_time_manual();
        $booked = null;
        if(!is_null($patient_data)) {
            $booked = Queue::where('queue_date', $now)->where('queue_patient_id', $patient_data->id)->first();
        }            
        $params = [
            'title' => "Jadwal",
            'now' => $now,
            'time' => $times_manual,
            'today_queue' => $today_queue,
            'patient_data' => $patient_data,
            'booked' => $booked,
            'today_male' => $today_male,
            'today_female' => $today_female,
        ];

        if(Session::get('activeUser')->user_role_id!=2) {
            if(Session::get('gender') == 'male'){
                return view('backend.schedule.index-male', $params);
            }
            // return response()->json($params);
            return view('backend.schedule.index-female', $params);
        }
        else {
            $user = Patient::where('patient_user_id', Session::get('activeUser')->id)->first();
            if($user->patient_gender =="laki_laki"){
                return view('backend_user.schedule.index-male', $params);
            }
            return view('backend_user.schedule.index-female', $params);
        }
    }

    public function scheduleMale(Request $request){
        return redirect('schedule')->with(['gender' => 'male']);
    }
    public function scheduleFemale(Request $request){
        return redirect('schedule')->with(['gender' => 'female']);
    }



    public function chart(Request $request){
        $now = date('Y-m');
        $last_education = LastEducation::all();
        $queue = Queue::selectRaw("date_format(queue_date, '%Y-%m') as month")->groupBy('month')->get();
        
        $data_month = session()->get('data_month');
        if(!is_null($data_month)){
            $now = session()->get('data_month');
        }

        $now2 = new DateTime($now);
        $year = $now2->format('Y');
        $month = $now2->format('m');

        $queue_month = Queue::select('queue_patient_id')->whereYear('queue_date', $year)->whereMonth('queue_date', $month)->where('queue_status_check',1)->get();
        $queue_list = Queue::whereYear('queue_date', $year)->whereMonth('queue_date', $month)->get();

        $queue_therapy_id = Queue::select('id')->whereYear('queue_date', $year)->whereMonth('queue_date', $month)->where('queue_status_check', 1)->get();
        $therapy_recap = TherapyRecap::whereIn('therapy_recap_queue_id', $queue_therapy_id)->get();

        $chart_gender=$this->gender($queue_month);
        $chart_therapy=$this->therapy($therapy_recap);
        $chart_last_education=$this->lastEducation($queue_month);

        $params=[
            'title' => "Grafik",
            'now' => $now,
            'queue' => $queue,
            'last_education' => $last_education,
            'chart_gender' => $chart_gender,
            'chart_therapy' => $chart_therapy,
            'chart_last_education' => $chart_last_education,
            'queue_list' => $queue_list,
            'queue_month' => $queue_month,
        ];

        // return response()->json($params);
        return view('backend.chart.index', $params);
    }

    public function chart2(Request $request){
        return redirect('chart')->with(['data_month' => $request->month]);
    }    


    //gender chart
    public function gender($queue_month){
        $chart_gender=[];
        $laki_laki=0;
        $perempuan=0;
        if(!is_null($queue_month)){
            foreach ($queue_month as $key => $value) {
                if($value->getPatient->patient_gender=="laki_laki"){
                    $laki_laki = $laki_laki+1;
                }else if($value->getPatient->patient_gender=="perempuan"){
                    $perempuan = $perempuan+1;
                }                
            }
        } 
        $chart_gender = [
            'laki_laki' => $laki_laki,
            'perempuan' => $perempuan
        ];
        return $chart_gender;
    }


    //therapy chart
    public function therapy($therapy_recap){
        $chart_therapy=[];
        $konsultasi=0;
        $bekam=0;
        $topung=0;
        $matalogi=0;
        // $lintah=0;
        $lain_lain=0;
        if(!is_null($therapy_recap)){
            foreach ($therapy_recap as $key => $value) {
                if($value->gettherapy->therapy_name=="bekam sunnah"){
                    $bekam = $bekam+1;
                }else if($value->gettherapy->therapy_name=="topung"){
                    $topung = $topung+1;
                }else if($value->gettherapy->therapy_name=="matalogi"){
                    $matalogi = $matalogi+1;
                }else if($value->gettherapy->therapy_name=="lain-lain"){
                    $lain_lain = $lain_lain+1;
                // }else if($value->gettherapy->therapy_name=="lintah"){
                //     $lintah = $lintah+1;
                }
            }
        } 
        $chart_therapy = [
            'bekam' => $bekam,
            'topung' => $topung,
            'matalogi' => $matalogi,
            // 'lintah' => $lintah,
            'lain_lain' => $lain_lain,
        ];
        return $chart_therapy;
    }

    
    //last education chart
    public function lastEducation($queue_month){
        $last_education = LastEducation::all();
        $chart_last_education = array();
        for ($i=0; $i <10 ; $i++) { 
            $chart_last_education[$i] = 0;
        }
        if(!is_null($queue_month)){
            foreach ($queue_month as $key => $value) {
                foreach($last_education as $l){
                    if($value->getPatient->patient_last_education_id == $l->id){
                        $chart_last_education[$l->id] = $chart_last_education[$l->id] + 1;
                    }
                }
            }
        } 
        return $chart_last_education;
    }


    //automatic time generator
    public function create_time_range($start, $end, $interval = '30 mins', $format = '12') {
        $startTime = strtotime($start); 
        $endTime   = strtotime($end);
        $returnTimeFormat = 'H:i';
    
        $current   = time(); 
        $addTime   = strtotime('+'.$interval, $current); 
        $diff      = $addTime - $current;
    
        $times = array(); 
        while ($startTime < $endTime) { 
            $times[] = date($returnTimeFormat, $startTime); 
            $startTime += $diff; 
        } 
        $times[] = date($returnTimeFormat, $startTime); 
        return $times; 
    }

    
    //manual time generator 
    public function create_time_manual($format = '12') {
        $timeFormat = 'H:i';
        $times = array(
            date($timeFormat, strtotime('08.00')),
            date($timeFormat, strtotime('08.30')),
            date($timeFormat, strtotime('09.00')),
            date($timeFormat, strtotime('09.30')),
            date($timeFormat, strtotime('10.00')),
            date($timeFormat, strtotime('10.30')),
            date($timeFormat, strtotime('11.00')),
            date($timeFormat, strtotime('11.30')),
            date($timeFormat, strtotime('12.00')),
            date($timeFormat, strtotime('12.30')),
            date($timeFormat, strtotime('13.00')),
            date($timeFormat, strtotime('13.30')),
            date($timeFormat, strtotime('14.00')),
            date($timeFormat, strtotime('14.30')),
            date($timeFormat, strtotime('15.00')),
            date($timeFormat, strtotime('15.30')),
            // date($timeFormat, strtotime('16.00')),
            // date($timeFormat, strtotime('16.30')),
        ); 
        return $times; 
    }



}