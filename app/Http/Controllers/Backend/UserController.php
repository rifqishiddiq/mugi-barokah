<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Services\UserService;
use App\Services\EmployeeService;
use App\Services\PatientService;
use App\Services\RegencyService;
use App\Services\JobService;
use App\Models\User;
use App\Models\Patient;
use App\Models\Employee;
use App\Models\LastEducation;
date_default_timezone_set('Asia/Jakarta');

class UserController extends Controller
{
    private $userService;
    private $employeeService;
    private $patientService;
    private $titlePage='User Table';
    private $view='backend.user';


    public function __construct()
    {
        $this->userService = new  UserService();
        $this->employeeService = new  EmployeeService();
        $this->patientService = new  PatientService();
        $this->regencyService = new  RegencyService();
        $this->jobService = new  JobService();
    }

    public function index(){
        $params=[
            'title' => $this->titlePage
        ];
        return view($this->view.'.index', $params);
    }

    public function show(Request $request){
        $id = $request->id;
        $data = $this->userService->findOne($id);
        if($data['data']->user_role_id == 1){
            // return response()->json($data);
            return redirect('/employee/detail/?id='.encrypt($data['data']->id));
        }
    }

    public function add(Request $request, $id){

        $rolename="";
        $role=$request->input('role');
        return response()->json($request->all());

        $user = User::find(decrypt($id));
        $employee = new Employee();
        $patient = new Patient();

        if(is_null($user)){
            $user = new User();
        }

        $regencyData= $this->regencyService->getData();
        $jobData= $this->jobService->getData();

        if ($role==1){
            $employee = Employee::where('employee_user_id', $user->id)->first();
            $rolename="employee";
        }
        else if ($role==2){
            $patient = Patient::where('patient_user_id', $user->id)->first();
            $rolename="patient";
        }

        $title = $rolename. " Form";

        $params=[
            'title'=>$title,
            'user'=>$user,
            'employee'=>$employee,
            'patient'=>$patient,
            'regencyData'=> $regencyData['data'],
            'jobData'=> $jobData['data'],
            'last_education' => LastEducation::all(),
        ];

        return response()->json($params);
        return view($this->view.'.'.$rolename.'.form-modal',$params);
    }

    public function save(Request $request){
        $userParams=[
            'id'=>$request->input('id'),
            'user_username' =>$request->input('username'),
            'user_password' =>Hash::make($request->input('password')),
            'user_role_id' =>$request->input('role'),
        ];
        $result = $this->userService->actionSave($userParams);

        if($result['code']==200 || $result['code']==302){
            if($request->input('role')=='1' || $request->input('role')==1){
                $employeeParams=[
                    'employee_name' =>$request->input('employee_name'),
                    'employee_position' =>$request->input('employee_position'),
                    'employee_gender' =>$request->input('employee_gender'),
                    'employee_regency_id' =>$request->input('employee_regency_id'),
                    'employee_birth_date' =>$request->input('employee_birth_date'),
                    'employee_phone_number' =>$request->input('employee_phone_number'),
                    'employee_email' =>$request->input('employee_email'),
                    'employee_address' =>$request->input('employee_address'),
                ];
                // return response()->json($employeeParams);
                $result2 = $this->employeeService->actionSave($employeeParams,$result['data']);
            }
            else if($request->input('role')=='2' || $request->input('role')==2){
                $patientParams=[
                    'patient_medical_id' =>$request->input('patient_medical_id'),
                    'patient_fullname' =>$request->input('patient_fullname'),
                    'patient_nickname' =>$request->input('patient_nickname'),
                    'patient_gender' =>$request->input('patient_gender'),
                    'patient_regency_id' =>$request->input('patient_regency_id'),
                    'patient_job_id' =>$request->input('patient_job_id'),
                    'patient_birth_date' =>$request->input('patient_birth_date'),
                    'patient_phone_number' =>$request->input('patient_phone_number'),
                    'patient_phone_number_2' =>$request->input('patient_phone_number_2'),
                    'patient_email' =>$request->input('patient_email'),
                    'patient_address' =>$request->input('patient_address'),
                    'patient_religion' =>$request->input('patient_religion'),
                    'patient_mariage_status' =>$request->input('patient_mariage_status'),
                    'patient_source_info' =>$request->input('patient_source_info'),
                    'patient_allergic_history' =>$request->input('patient_allergic_history'),
                ];
                $result2 = $this->patientService->actionSave($patientParams,$result['data']);
            }
            return "
                <div class='alert alert-success center-alert'>".$result['message']."</div>
                <script> scrollToTop(); reload(1000); </script>";

        }else{

            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }

    }

    public function delete(Request $request){
        $id = $request->input('id');
        $result = $this->userService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1000); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function dataTable(Request $request)
    {
        return $this->userService->actionDataTable($request);
    }

}