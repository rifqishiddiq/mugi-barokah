<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\QueueService;
use Session;
use DateTime;
use App\Models\Patient;
use App\Models\Queue;
date_default_timezone_set('Asia/Jakarta');

class QueueController extends Controller
{
    private $employeeService;
    private $titlePage='Tabel Antrian';
    private $view='backend.queue';

    public function __construct()
    {
        $this->queueService = new  QueueService();
    }

    
    public function index(){
        if(Session::get('activeUser')->user_role_id==2){
            $patient = Patient::where('patient_user_id', Session::get('activeUser')->id)->first();
            $params=[
                'patient' => $patient,
                'title' => $this->titlePage
            ];
            return view('backend_user.queue.index', $params);
        }
        $params=[
            'title' => $this->titlePage
        ];
    return view($this->view.'.index', $params);
    }


    public function detail(Request $request, $id){        

        $queue_id = decrypt($id);
        $queue = Queue::find($queue_id);
        // if(is_null($queue)){
        //     $num = $queue_id;
        //     $num = $num + 1;
        //     $queue = Queue::find($num);
        // }

        $set = 0;
        $now = date("Y-m-d H:i:s");
        $now2 = strtotime($now);
        $now_date = strtotime("$queue->queue_date $queue->queue_time");

        if($now2 < $now_date){
            $set = 1;
        }


        $title = "Detail Antrian";
        $params=[
            'set' => $set,
            'title' => $title,
            'queue' => $queue,
            'queue_id' => $queue_id,
            'id' => $id,
        ];

        // return response()->json($params);
        // return view($this->view.'.index', $params);
        return view($this->view.'.show', $params);
    }


    public function add(Request $request){
        $queue = new Queue();
        if(isset($request->queue_id)){
            // if($request)
            $queue = Queue::find(decrypt($request->queue_id));
        }
        $now = date('Y-m-d');
        $patient = Patient::all();
        $times_auto = app(\App\Http\Controllers\Backend\BackendController::class)->create_time_manual();
        // $times_auto = $this->create_time_range('08:00', '15:30', '30 mins');
        $params=[
            'now' => $now,
            'queue' => $queue,
            'patient' => $patient,
            'time' => $times_auto,
            'title' => $this->titlePage
        ];
        if(isset($request->gender)){
            if($request->gender==1){
                return view($this->view.'.form-female', $params);
            }
            return view($this->view.'.form-male', $params);
        }
        return view($this->view.'.form', $params);
    }
    
    
    public function save(Request $request){
        // return response()->json($request->all());
        $now = date('Y-m-d');
        $now2 = date('Y-m-d H:i:s');
        $now_datetime = date('Y-m-d H:i:s');
        $now3 = date('Y-m-d H:i:s');

        $patient_data = Patient::where('patient_user_id', Session::get('activeUser')->id)->first();
        if(isset($request->queue_date)){
            $queue_time = strtotime($request->queue_time);
            $queue_time = date("H:i:s", $queue_time);
            $queue_date = $request->queue_date;
            // $now3 = new DateTime($queue_date .' ' .$queue_time);
            $now3 = $queue_date .' ' .$queue_time;
            if(isset($request->patient_id)){
                $patient_data = Patient::find($request->patient_id);
            }
        } else{
            $datetime = $request->input('datetime');
            $queue_time = date("H:i:s", $datetime);
            $queue_date = date("Y-m-d", $datetime);
            $now3 = $queue_date .' ' .$queue_time;
        }

        // return response()->json($patient_data);
        if(Session::get('activeUser')->user_role_id==2){
            if($queue_date == $now){
                if(strtotime($now_datetime) > strtotime($now3)){
                    return "<div class='alert alert-danger center-alert'>waktu sudah terlewati</div>";
                }
            }
        }

        $booked2 = Queue::where('queue_date', $queue_date)
            ->where('queue_time', $queue_time)->first();

        if(!is_null($booked2)){
            if($booked2->queue_patient_id !== $patient_data->id){
                if($booked2->getPatient->patient_gender == $patient_data->patient_gender){
                    return "<div class='alert alert-danger center-alert'>Tanggal dan Jam tersebut sudah dipesan</div>";
                }
            }
        }
        
        $queue = Queue::find(decrypt($request->queue_id));
        if(is_null($queue)){
            $id = $request->queue_id;
        } else{
            $id = encrypt($queue->id);
        }
        
        $queue_status_check = 2;
        if(isset($request->queue_date)){
            $booked_date = Queue::where('queue_date', $queue_date)
            ->where('queue_patient_id', $patient_data->id)->first();
            
            if(!is_null($booked_date)){
                if($booked_date->queue_status_check==1) {
                    return "<div class='alert alert-danger center-alert'>data sudah selesai diproses</div>";
                } elseif($booked_date->queue_status_check==3) {
                    return "<div class='alert alert-danger center-alert'>data sedang diproses</div>";
                }
                $id = encrypt($booked_date->id);
            }
            
        } else{
            $booked = Queue::where('queue_date', $now)
                ->where('queue_patient_id', $patient_data->id)->first();            
            if(!is_null($booked)){
                if($booked->queue_status_check==1) {
                    return "<div class='alert alert-danger center-alert'>data sudah selesai diproses</div>";
                } elseif($booked->queue_status_check==3) {
                    return "<div class='alert alert-danger center-alert'>data sedang diproses</div>";
                }
                $id = encrypt($booked->id);
            }
        }

        
        $queue_patient_id = $patient_data->id;
        $queue_note = $request->queue_note;
        $params = [
            'id'=> $id,
            'queue_patient_id' => $patient_data->id,
            'queue_time' => $queue_time,
            'queue_date' => $queue_date,
            'queue_status_check' => $queue_status_check,
            'queue_note' => $queue_note,
            // 'now_datetime' => $now_datetime,
        ];

        // return response()->json($params);
     
        $result = $this->queueService->actionSave($params);
        if($result['code'] == 200 || $result['code']==302){
            return "
                <div class='alert alert-success center-alert'>".$result['message']."</div>
                <script> scrollToTop(); reload(1500); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
        // return response()->json($params);
    }

    public function process(Request $request){
        // return response()->json($request->all());
        $id = $request->input('id');
        $queue = Queue::find($id);
        $queue->queue_status_check = 3;
        $queue->save();

        $params = [
            'queue_data' => $queue,
        ];
        return "
        <div class='alert alert-success center-alert'>Pesanan Diproses</div>
        <script> scrollToTop(); reload(1000); </script>";
    }

    public function approve(Request $request){
        $id = $request->input('id');
        $queue = Queue::find($id);
        // return response()->json($queue);
        $queue->queue_status_check = 1;
        $queue->save();

        $params = [
            'queue_data' => $queue,
        ];
        return "
        <div class='alert alert-success center-alert'>Pesanan Disetujui</div>
        <script> scrollToTop(); reload(1000); </script>";
    }

    public function decline(Request $request){
        $id = $request->input('id');
        $queue = Queue::find($id);
        // return response()->json($queue);
        $queue->queue_status_check = 2;
        $queue->save();

        $params = [
            'queue_data' => $queue,
        ];
        return "
        <div class='alert alert-success center-alert'>Pesanan Dibatalkan</div>
        <script> scrollToTop(); reload(1000); </script>";
    }

    public function delete(Request $request){
        $id = decrypt($request->input('id'));
        $data = Queue::find($id);
        // return response()->json($id);
        if($data->queue_status_check!=2){
            return "<div class='alert alert-danger center-alert'>data tidak bisa dihapus (sedang diproses atau sudah selesai diproses)</div>";
        }
        $result = $this->queueService->actionDelete(encrypt($id));
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1000); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }


    public function dataTable(Request $request)
    {
        $patientId = $request->patient_id;
        // return response()->json($patientId);
        return $this->queueService->actionDataTable($request,$patientId);
    }
 

}
