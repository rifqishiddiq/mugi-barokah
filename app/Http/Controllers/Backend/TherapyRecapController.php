<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TherapyRecap;
use App\Models\Therapy;
use App\Models\Patient;
use App\Models\Employee;
use App\Models\Queue;
use Session;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\Services\TherapyRecapService;

class TherapyRecapController extends Controller
{

    private $therapyRecapService;
    private $titlePage='Tabel Rekam Medis';
    private $view='backend.therapy-recap';

    public function __construct()
    {
        $this->therapyRecapService = new  TherapyRecapService();
    }

    
    public function index(){
        $therapy = Therapy::all(); 
        $params=[
            'title' => $this->titlePage,
            'therapy' => $therapy,
        ];
        if(Session::get('activeUser')->user_role_id==2){
            $patient = Patient::where('patient_user_id', Session::get('activeUser')->id)->first();
            $params=[
                'title' => $this->titlePage,
                'therapy' => $therapy,
                'patient' => $patient
            ];
            // return response()->json($params);
            return view('backend_user.therapy-recap.index', $params);
        }
        // return response()->json($params);
        return view($this->view.'.index', $params);
    }


    public function detail(Request $request, $id){        
        $id = decrypt($id);
        // return response()->json($id);
        
        $therapy_recap=TherapyRecap::find($id);
        $queue = Queue::find($therapy_recap->therapy_recap_queue_id);
        
        $patient=Patient::find($queue->queue_patient_id);
        $title = "Detail Rekam Medis";
        $params=[
            'title' => $title,
            'queue' => $queue,
            'patient' => $patient,
            'therapy_recap' => $therapy_recap,
            'id' => $id,
        ];

        // return response()->json($params);
        return view($this->view.'.medical-record', $params);
    }

    public function add(Request $request, $id){
        // return response()->json($id);
        $therapy = Therapy::all();
        $therapist = Employee::where('employee_position', "like","%therapist%")->get();
        $therapy_recap = TherapyRecap::where('id', $id)->first();
        $title = "Tambah Rekam Medis";
        if(is_null($therapy_recap)){
            $queue = Queue::find(decrypt($id));
            $therapy_recap = new TherapyRecap();
        }else{
            $title = "Edit Rekam Medis";
            $queue = Queue::find($therapy_recap->therapy_recap_queue_id);
        }

        $params=[
            'title' => $title,
            'queue' => $queue,
            'therapy_recap' => $therapy_recap,
            'therapist' => $therapist,
            'therapy' => $therapy,
        ];

        // return response()->json($params);
        return view($this->view.'.form',$params);
    }


    public function save(Request $request){
        $therapy_recap_id = decrypt($request->therapy_recap_id);
        $therapy_recap = TherapyRecap::find($therapy_recap_id);
        $therapy_recap_subsidy = $request->therapy_recap_subsidy;
        if(is_null($request->therapy_recap_subsidy)){
            $therapy_recap_subsidy = 0;
        }
        if(is_null($therapy_recap)){
            $therapy_recap = new TherapyRecap();
        }
        $therapy_recap->therapy_recap_queue_id = $request->queue_id;
        $therapy_recap->therapy_recap_therapy_id = $request->therapy_recap_therapy_id;
        $therapy_recap->therapy_recap_therapist_id = $request->therapy_recap_therapist_id;
        $therapy_recap->therapy_recap_complaint = $request->therapy_recap_complaint;
        $therapy_recap->therapy_recap_result = $request->therapy_recap_result;
        $therapy_recap->therapy_recap_receipt = $request->therapy_recap_receipt;
        $therapy_recap->therapy_recap_price = $request->therapy_recap_price;
        $therapy_recap->therapy_recap_subsidy = $therapy_recap_subsidy;
        $therapy_recap->therapy_recap_total_price = $request->therapy_recap_price - $therapy_recap_subsidy;

        // return response()->json($therapy_recap);
        $therapy_recap->save();

        $queue = Queue::find($request->queue_id);
        $queue->queue_status_check=1;
        $queue->save();

        return redirect()->to('medical-record/detail/'. encrypt($therapy_recap->id) )->with('msg', 'Add Data Success');
        // return view($this->view.'.form',$params);
    }

    public function dataTable(Request $request)
    {

        // $patientId=$request->patient_id;
        // return $this->therapyRecapService->actionDataTable($request,$patientId);

        DB::statement(DB::raw('set @rownum=0'));

        if(isset($request->patient_id)){
        
            $therapy_recap = TherapyRecap::select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                'therapy_recaps.id',
                'therapy_recaps.therapy_recap_complaint',
                'therapy_recaps.therapy_recap_queue_id',
                'therapy_recaps.therapy_recap_therapy_id',
                'therapy_recaps.therapy_recap_result',
                'therapy_recaps.therapy_recap_receipt',
                'therapy_recaps.therapy_recap_price',
                'therapy_recaps.therapy_recap_subsidy',
                'queues.queue_date',
                'queues.queue_time',
                'queues.queue_patient_id',            
                'queues.queue_status_check',
                'therapies.therapy_name',
                'employees.employee_name',
            ])->join('queues','queues.id','=','therapy_recaps.therapy_recap_queue_id')
            ->join('therapies','therapies.id','=','therapy_recaps.therapy_recap_therapy_id')
            ->join('employees','employees.id','=','therapy_recaps.therapy_recap_therapist_id')
            ->where('queues.queue_patient_id', $request->patient_id)
            ->orderBy('queues.queue_date', 'DESC')
            ->orderBy('queues.queue_time', 'DESC')->get();
            
            $i = 1;
            foreach($therapy_recap as $t){
                $t->rownum = $i;
                $i++;
            }
    
            return Datatables::of($therapy_recap)
                ->addColumn('action', function ($therapy_recap) {
                    return "<a class='btn btn-sm btn-primary' href='".url('medical-record/detail', encrypt($therapy_recap->id))."' title='Detail'>
                            <span class='btn-inner--text' style='text-align: center; padding: 0'>Detail</span></a>";})
                            
                ->make(true);
                
        }

        $therapy_recap = TherapyRecap::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'therapy_recaps.id',
            'therapy_recaps.therapy_recap_complaint',
            'therapy_recaps.therapy_recap_queue_id',
            'therapy_recaps.therapy_recap_therapy_id',
            'therapy_recaps.therapy_recap_result',
            'therapy_recaps.therapy_recap_receipt',
            'therapy_recaps.therapy_recap_price',
            'therapy_recaps.therapy_recap_subsidy',
            'queues.queue_date',
            'queues.queue_time',
            'queues.queue_patient_id',            
            'queues.queue_status_check',
            'patients.patient_fullname',
            'therapies.therapy_name',
            'employees.employee_name',
        ])->join('queues','queues.id','=','therapy_recaps.therapy_recap_queue_id')
        ->join('therapies','therapies.id','=','therapy_recaps.therapy_recap_therapy_id')
        ->join('employees','employees.id','=','therapy_recaps.therapy_recap_therapist_id')
        ->join('patients','patients.id','=','queues.queue_patient_id')
        ->orderBy('queues.queue_date', 'DESC')
        ->orderBy('queues.queue_time', 'DESC')->get();
        
        $i = 1;
        foreach($therapy_recap as $t){
            $t->rownum = $i;
            $i++;
        }

        return Datatables::of($therapy_recap)
            ->addColumn('action', function ($therapy_recap) {
                return "<a class='btn btn-sm btn-primary' href='".url('medical-record/detail', encrypt($therapy_recap->id))."' title='Detail'>
                        <span class='btn-inner--text' style='text-align: center; padding: 0'>Detail</span></a>";})
                        
            ->make(true);
    }


    public function dataTable2(Request $request)
    {
        $patientId=$request->patient_id;
        return $this->therapyRecapService->actionDataTable($request,$patientId);
    }

}
