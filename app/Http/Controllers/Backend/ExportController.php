<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Queue;
use App\Models\TherapyRecap;
use App\Models\PatientTherapyReport;
use PDF;
use Excel;
use App\Exports\MonthlyReport;
use DateTime;
date_default_timezone_set('Asia/Jakarta');

class ExportController extends Controller
{
    public function medicalRecordPDF(Request $request, $id){
        $id = decrypt($id);
        $therapy_recap = TherapyRecap::find($id);
        $now = date('Y-m-d');
        $queue = Queue::find($therapy_recap->therapy_recap_queue_id);

        $params = [
            'now' => $now,
            'queue' => $queue,
            'therapy_recap' => $therapy_recap,
            'title' => 'Medical Report'
        ];
 
        $pdf = PDF::loadView('export.medical-record', $params);
        return $pdf->stream();
    }
    

    public function monthlyReportExcel($date){
        $now = date($date);
        $now2 = new DateTime($now);
        $year = $now2->format('Y');
        $month = $now2->format('m');

        $data = DB::table('patient_therapy_reports')
            ->whereYear('queue_date', $year)
            ->whereMonth('queue_date', $month)
            ->get();
        
        return Excel::download(new MonthlyReport($data), 'laporan '. $month .'-'. $year .'.xlsx');    
    }

}