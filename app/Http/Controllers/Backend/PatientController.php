<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Services\PatientService;
use App\Services\UserService;
use App\Models\TherapyRecap;
use App\Models\Queue;
use App\Models\Patient;
use App\Models\Regency;
use App\Models\User;
use App\Models\LastEducation;

class PatientController extends Controller
{
    private $userService;
    private $employeeService;
    private $patientService;
    private $titlePage='Tabel Klien';
    private $view='backend.user.patient';


    public function __construct()
    {
        $this->patientService = new  PatientService();
        $this->userService = new  UserService();
    }

    
    public function index(){
        $params=[
            'title' => $this->titlePage
        ];
        return view($this->view.'.index', $params);
    }


    public function show($id){
        $patient = $this->patientService->findOne($id);
        $params=[
            'patient' => $patient['data'],
            'title' => 'Detail Klien'
        ];
        // return response()->json($params);
        return view($this->view.'.show', $params);
    }


    public function add(Request $request, $id){
        
        // return response()->json($id);
        if($id == "add"){
            $user = new User();
            $patient = new Patient();
        } else{
            $user = User::find($id);
            $patient = Patient::where('patient_user_id', $user->id)->first();
        }

        $regency = Regency::all();
        $last_education = LastEducation::all();
        
        $religion = array('islam', 'kristen-protestan', 'katolik', 'budha', 'hindu');
        $mariage_status = array('menikah', 'lajang', 'janda', 'duda');
        $source_info = array('internet', 'saudara', 'teman', 'radio');
        
        $params=[
            'title'=>'Edit Klien',
            'user'=>$user,
            'patient'=>$patient,
            'regencyData'=> $regency,
            'last_education' => $last_education,
            'religion' => $religion,
            'mariage_status' => $mariage_status,
            'source_info' => $source_info
        ];

        // return response()->json($params);
        return view($this->view.'.form',$params);
    }


    public function save(Request $request){
        $user = User::find(decrypt($request->input('user_id')));        
        $regency_id = $request->input('patient_regency_id');
        if(is_null($request->patient_regency_id)){
            $regency_id = Regency::select('id')->where('regency_name', $request->birthplace)->first();
            $regency_id = $regency_id['id'];
        }

        // return response()->json($regency_id);
        
        $new_password = $request->input('user_password'); 
        if(is_null($user)){
            $user = new User();
            $new_password = Hash::make($request->input('user_password')); 
        } else{
            if($user->user_password!==$new_password){
                $new_password = Hash::make($new_password);
            }    
        }
        $user->user_username = $request->input('user_username');
        $user->user_password = $new_password;
        $user->user_role_id = $request->input('user_role_id');
        $user->save();

        $patient = Patient::where('patient_user_id', $user->id)->first();
        if(is_null($patient)){
           $patient = new Patient(); 
        }
        $patient->patient_user_id = $user->id;
        $patient->patient_medical_id = $request->input('patient_medical_id');
        $patient->patient_fullname = $request->input('patient_fullname');
        $patient->patient_nickname = $request->input('patient_nickname');
        $patient->patient_gender = $request->input('patient_gender');
        $patient->patient_regency_id = $regency_id;
        $patient->patient_last_education_id = $request->input('patient_last_education_id');
        $patient->patient_job = $request->input('patient_job');
        $patient->patient_birth_date = $request->input('patient_birth_date');
        $patient->patient_phone_number = $request->input('patient_phone_number');
        $patient->patient_phone_number_2 = $request->input('patient_phone_number_2');
        $patient->patient_email = $request->input('patient_email');
        $patient->patient_address = $request->input('patient_address');
        $patient->patient_religion = $request->input('patient_religion');
        $patient->patient_mariage_status = $request->input('patient_mariage_status');
        $patient->patient_source_info = $request->input('patient_source_info');
        $patient->patient_allergic_history = $request->input('patient_allergic_history');
        $patient->save();

        return redirect()->to('/patient/detail/'. encrypt($patient->id));

    }


    public function medicalRecord($id){
        $patient = $this->patientService->findOne(encrypt($id));
        $params=[
            'title' => 'Rekam Medis Klien',
            'patient' => $patient['data'],
        ];
        // return response()->json($params);
        return view($this->view.'.medical-record', $params);
    }


    public function delete(Request $request){
        $id = $request->input('id');
        $result = $this->userService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1000); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }


    public function chartGender(Request $request)
    {
        $now = date('Y-m-d');
        $queue = Queue::select('queue_patient_id')->where('queue_date', $now)->where('queue_status_check', 1)->get();
        // $therapyRecap = TherapyRecap::select('therapy_recap_patient_id')->whereIn('therapy_recap_queue_id', $queue)->get();
        $patient = Patient::whereIn('id', $queue)->get();

        $laki_laki=0;
        $perempuan=0;

        if(is_null($patient)){
        } else {
            foreach ($patient as $key => $value) {

                if($value->patient_gender=="male" || $value->patient_gender=="laki_laki" || $value->patient_gender=="L"){
                    $laki_laki = $laki_laki+1;
                    $perempuan = $perempuan;
                }else if($value->patient_gender=="female" || $value->patient_gender=="perempuan" || $value->patient_gender=="P"){
                    $laki_laki = $laki_laki;
                    $perempuan = $perempuan+1;
                }
            }
        }
 
 
        $chartGender[] = [
            'laki_laki' => $laki_laki,
            'perempuan' => $perempuan,
        ];
 
        $params = [
            'data' => $chartGender,
        ];

        return response()->json($params);
    }


    public function dataTable(Request $request)
    {
        return $this->patientService->actionDataTable($request);
    }

}