<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RegencyService;

class RegencyController extends Controller
{
    private $regencyService;
    private $titlePage='Regency Table';
    private $view='backend.regency';


    public function __construct()
    {
        $this->regencyService = new  RegencyService();
    }

    public function index(){
        $params=[
            'title' => $this->titlePage
        ];

        // $regencyData= $this->regencyService->getData();
        // $regencyData= Regency::all();
        // return response()->json($regencyData);
        return view($this->view.'.index', $params);
    }

    public function delete(Request $request){
        $id = $request->input('id');
        $result = $this->regencyService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1000); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function dataTable(Request $request)
    {
        return $this->regencyService->actionDataTable($request);
    }

}