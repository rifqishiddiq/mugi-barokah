<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Services\EmployeeService;
use App\Models\User;
use App\Models\Employee;
use App\Models\Regency;

class EmployeeController extends Controller
{
    private $userService;
    private $employeeService;
    private $patientService;
    private $titlePage='Tabel Pegawai';
    private $view='backend.user.employee';


    public function __construct()
    {
        $this->employeeService = new  EmployeeService();
    }

    public function index(){
        $params=[
            'title' => $this->titlePage
        ];

        return view($this->view.'.index', $params);
    }

    public function show($id){
        $employee = $this->employeeService->findOne($id);
        $params=[
            'employee' => $employee['data'],
            'title' => $this->titlePage
        ];
        // return response()->json($params);
        return view($this->view.'.show', $params);
    }

    public function add(Request $request, $id){
        
        // return response()->json($id);
        if($id == "add"){
            $user = new User();
            $employee = new Employee();
        } else{
            $user = User::find($id);
            $employee = Employee::where('employee_user_id', $user->id)->first();
        }
        $regency = Regency::all();
        
        $params=[
            'title'=>$this->titlePage,
            'user'=>$user,
            'employee'=>$employee,
            'regency' =>$regency,
        ];

        // return response()->json($params);
        return view($this->view.'.form',$params);
    }


    public function save(Request $request){

        // return response()->json($request->all());   
        $user = User::find(decrypt($request->input('user_id')));
        
        
        $regency_id = $request->input('employee_regency_id');
        if(is_null($request->employee_regency_id)){
            $regency_id = Regency::select('id')->where('regency_name', $request->birthplace)->first();
            $regency_id = $regency_id['id'];
        }
        
        $new_password = $request->input('user_password'); 
        if(is_null($user)){
            $user = new User();
        } else{
            if($user->user_password!==$new_password){
                $new_password = Hash::make($new_password);
            }    
        }
        $user->user_username = $request->input('user_username');
        $user->user_password = $new_password;
        $user->user_role_id = $request->input('user_role_id');
        $user->save();

        $employee = Employee::where('employee_user_id', $user->id)->first();
        if(is_null($employee)){
           $employee = new Employee(); 
        }

        $employee->employee_user_id = $user->id;
        $employee->employee_name = $request->input('employee_name');
        $employee->employee_position = $request->input('employee_position');
        $employee->employee_gender = $request->input('employee_gender');
        $employee->employee_regency_id = $regency_id;
        $employee->employee_birth_date = $request->input('employee_birth_date');
        $employee->employee_phone_number = $request->input('employee_phone_number');
        $employee->employee_email = $request->input('employee_email');
        $employee->employee_address = $request->input('employee_address');
        $employee->save();

        return redirect()->to('/employee/detail/'. encrypt($employee->id));

    }



    public function delete(Request $request){
        $id = $request->input('id');
        $result = $this->employeeService->actionDelete($id);
        if($result['code'] == 202){
            return "
            <div class='alert alert-success center-alert'>".$result['message']."</div>
            <script> scrollToTop(); reload(1000); </script>";
        }else{
            return "<div class='alert alert-danger center-alert'>".$result['message']."</div>";
        }
    }

    public function dataTable(Request $request)
    {
        return $this->employeeService->actionDataTable($request);
    }

}