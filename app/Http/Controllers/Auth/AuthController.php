<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Session;

class AuthController extends Controller
{

    public function redirectToProvider($provider)
    {
        // return response()->json($provider);
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
         * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        // $params =[
        //     'user' => $user,
        //     'provider' => $provider,
        // ];
        // return response()->json($params);
        $authUser = $this->findOrCreateUser($user, $provider);
        // Auth::login($authUser, true);
        request()->session()->put('activeUser', $authUser);
        return redirect('/');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {

        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        else{
            $data = User::create([
                // 'user_username' => $user->name,
                'user_username' => !empty($user->email)? $user->email : '' ,
                // 'email' => !empty($user->email)? $user->email : '' ,
                'provider' => $provider,
                'provider_id' => $user->id,
                'user_role_id' => 2,
            ]);
            return $data;
            // return response()->json($data);
        }
    }
}