<?php

namespace App\Http\Middleware;
use Illuminate\Http\Response;

class NotAuthenticated
{
    public function handle(\Illuminate\Http\Request $request, \Closure $next)
    {
        if (empty(session('activeUser'))) {
            return redirect('/login');
        } else {
            if(session('activeUser')->user_role_id==2){
                return response(view('404'));
            }
            else {
                return $next($request);
            }
        }
    }
}