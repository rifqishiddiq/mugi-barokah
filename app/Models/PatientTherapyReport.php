<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PatientTherapyReport extends Model
{
	protected $table = 'patient_therapy_reports';
    // protected $primaryKey = 'id';
    
    // protected $fillable = [
    //     'last_education_name',
    // ];

    // public $rules=[
    //     'last_education_name' =>'required',
    // ];

    public $timestamps = false;
        
    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'patient_medical_id',
            'patient_fullname',
            'patient_phone_number',
            'patient_address',
            'patient_last_education_name',
            'therapy_recap_complaint',
            'therapy_name',
            'queue_date',
            'queue_time',
        ]);
    }
}
