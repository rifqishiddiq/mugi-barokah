<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Queue extends Model
{
	protected $table = 'queues';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'queue_patient_id', 
        'queue_date', 
        'queue_time', 
        'queue_status_check', 
        'queue_note', 
    ];

    public $rules=[
        'queue_patient_id' =>'required| exists:patients,id',
        'queue_date' =>'required',
        'queue_time' =>'required',
        'queue_status_check' => 'required',
    ];

    public $timestamps = false;
        
    public static function dataTable($patientId)
    {
        if(is_null($patientId)){
            DB::statement(DB::raw('set @rownum=0'));
            return self::select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                'queues.id',
                'queues.queue_date',
                'queues.queue_time',
                'queues.queue_patient_id',            
                'queues.queue_status_check',            
                'queues.queue_note',
                'patients.patient_fullname',
                'patients.patient_phone_number',
                // 'therapy_recaps.therapy_recap_complaint',
            ])->join('patients','patients.id','=','queues.queue_patient_id')
            // ->join('queues','queues.id','=','therapy_recaps.therapy_recap_queue_id')
            // ->whereIn('queue_status_check',[2,3])
            ;
        }
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'queues.id',
            'queues.queue_date',
            'queues.queue_time',
            'queues.queue_patient_id',            
            'queues.queue_status_check',
            'queues.queue_note',
        ])->where('queue_patient_id', $patientId)
        ->whereIn('queue_status_check',[2,3]);
    }

    public function getPatient() 
    {
        return $this->hasOne('App\Models\Patient', 'id', 'queue_patient_id');
    }

}
