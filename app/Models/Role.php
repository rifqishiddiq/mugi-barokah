<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{    
    protected $table = 'roles';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'role_name', 
    ];

    public $rules=[
        'role_name' =>'required',
    ];

    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'role_name',
        ]);
    }

}
