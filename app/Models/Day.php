<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Day extends Model
{
	protected $table = 'days';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'day_name', 
    ];

    public $rules=[
        'day_name' =>'required',
    ];

    public $timestamps = false;
        
    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'days.id',
            'day_name',
        ]);
    }
}
