<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Notifications\Notifiable;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    // use Notifiable;
    
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'user_username', 
        'user_password', 
        'user_role_id',
        'provider',
        'provider_id',
        'remember_token',
    ];

    public $rules=[
        'user_username' =>'required| unique:users',
        'user_password' =>'required',
        'user_role_id' => 'required| exists:roles,id',
    ];

    protected $hidden = [
        'user_password'
    ];

    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'users.id',
            'user_username',
            'role_name'
        ])->join('roles','roles.id','=','users.user_role_id');
    }

}
