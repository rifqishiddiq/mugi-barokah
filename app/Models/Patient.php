<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Patient extends Model
{
	protected $table = 'patients';
    protected $primaryKey = 'id';

    protected $fillable = [
        'patient_user_id', 
        'patient_medical_id', 
        'patient_fullname', 
        'patient_nickname', 
        'patient_gender', 
        'patient_regency_id', 
        'patient_last_education_id',
        'patient_birth_date', 
        'patient_job', 
        'patient_phone_number', 
        'patient_phone_number_2', 
        'patient_email', 
        'patient_address',
        'patient_religion',
        'patient_mariage_status',
        'patient_source_info',
    ];

    public $rules=[
        'patient_user_id' =>'required| exists:users,id',
        'patient_medical_id' =>'required| unique:patients',
        'patient_fullname' =>'required',
        'patient_nickname' => 'required',
        'patient_patient_gender' =>'required',
        'patient_regency_id' => 'required| exists:regencies,id',
        'patient_last_education_id' => 'required| exists:last_educations,id',
        'patient_job' => 'required',
        'patient_birth_date' =>'required',
        'patient_phone_number' => 'required',
        'patient_email' =>'required| email| unique:patients',
        'patient_address' => 'required',
        'patient_religion' => 'required',
        'patient_mariage_status' =>'required',
        'patient_allergic_history' => 'required',
    ];

    public $timestamps = true;
        
    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'patients.id',
            'patient_medical_id',
            'patient_user_id',
            'patient_fullname',
            'patient_nickname',
            'patient_gender',
            'patient_regency_id',
            'patient_last_education_id',
            'patient_job',
            'patient_birth_date',
            'patient_phone_number',
            'patient_phone_number_2',
            'patient_email',
            'patient_address',
            'patient_religion',
            'patient_mariage_status',
            'patient_source_info',
            'patient_allergic_history',
        ])->join('users','users.id','=','patients.patient_user_id');
    }

    public function getUser(){
        return $this->hasOne('App\Models\User', 'id', 'patient_user_id');
    }

    public function getRegency(){
        return $this->hasOne('App\Models\Regency', 'id', 'patient_regency_id');
    }

    public function getLastEducation(){
        return $this->hasOne('App\Models\LastEducation', 'id', 'patient_last_education_id');
    }
}
