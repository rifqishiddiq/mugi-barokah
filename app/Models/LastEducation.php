<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LastEducation extends Model
{
	protected $table = 'last_educations';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'last_education_name',
    ];

    public $rules=[
        'last_education_name' =>'required',
    ];

    public $timestamps = false;
        
    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'last_education_name',
        ]);
    }
}
