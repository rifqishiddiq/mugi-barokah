<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Regency extends Model
{
	protected $table = 'regencies';
    protected $primaryKey = 'id';

    protected $fillable = [
        'regency_province_id', 
        'regency_name'
    ];

    public $rules=[
        'regency_province_id' =>'required| exists:provinces,id',
        'regency_name' =>'required',
    ];

    public $timestamps = false;
        
    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'regencies.id',
            'provinces.province_name',
            'regency_name',
        ])->join('provinces','provinces.id','=','regencies.regency_province_id');
    }

}
