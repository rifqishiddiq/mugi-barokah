<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
	protected $table = 'employees';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'employee_user_id', 
        'employee_name', 
        'employee_position', 
        'employee_gender', 
        'employee_regency_id', 
        'employee_birth_date', 
        'employee_phone_number', 
        'employee_email', 
        'employee_address',
        'employee_allergic_history',
    ];

    public $rules=[
        'employee_user_id' =>'required| exists:users,id',
        'employee_name' =>'required',
        'employee_position' => 'required',
        'employee_gender' =>'required',
        'employee_regency_id' => 'required| exists:regencies,id',
        'employee_birth_date' =>'required',
        'employee_phone_number' => 'required',
        'employee_email' =>'required| email| unique:employees',
        'employee_address' => 'required',
        'employee_allergic_history' => 'required',
    ];

    public $timestamps = true;
        
    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'employees.id',
            'employee_user_id',
            'employee_name',
            'employee_position',
            'employee_gender',
            'employee_regency_id',
            'employee_birth_date',
            'employee_phone_number',
            'employee_email',
            'employee_address',
        ])->join('users','users.id','=','employees.employee_user_id');
    }

    public function getUser(){
        return $this->hasOne('App\Models\User', 'id', 'employee_user_id');
    }

    public function getRegency(){
        return $this->hasOne('App\Models\Regency', 'id', 'employee_regency_id');
    }

}
