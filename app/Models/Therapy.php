<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Therapy extends Model
{
	protected $table = 'therapies';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'therapy_name',
    ];

    public $rules=[
        'therapy_name' =>'required',
    ];

    public $timestamps = false;
        
    public static function dataTable()
    {
        DB::statement(DB::raw('set @rownum=0'));
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'therapy_name',
        ]);
    }
}
