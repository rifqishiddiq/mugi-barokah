<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TherapyRecap extends Model
{
	protected $table = 'therapy_recaps';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'therapy_recap_queue_id', 
        'therapy_recap_therapy_id',
        'therapy_recap_therapist_id',
        'therapy_recap_complaint',
        'therapy_recap_result',
        'therapy_recap_receipt',
        'therapy_recap_time',
        'therapy_recap_price',
        'therapy_recap_subsidy',
        'therapy_recap_total_price',
    ];

    public $rules=[
        'therapy_recap_queue_id' =>'required| exists:queues,id', 
        'therapy_recap_therapy_id' =>'required| exists:therapist,id',
        'therapy_recap_therapist_id' =>'required| exists:employees,id',
        'therapy_recap_complaint' =>'required',
        'therapy_recap_result' =>'required',
        'therapy_recap_receipt' =>'required',
        'therapy_recap_time' =>'required',
        'therapy_recap_price' =>'required',
        // 'subsidy' =>'required'
    ];

    public $timestamps = false;

    public static function dataTable($patientId)
    {
        DB::statement(DB::raw('set @rownum=0'));
        if(is_null($patientId)){
            return self::select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                'therapy_recaps.id',
                'therapy_recaps.therapy_recap_complaint',
                'therapy_recaps.therapy_recap_queue_id',
                'therapy_recaps.therapy_recap_therapy_id',
                'therapy_recaps.therapy_recap_therapist_id',
                'therapy_recaps.therapy_recap_result',
                'therapy_recaps.therapy_recap_receipt',
                'therapy_recaps.therapy_recap_price',
                'therapy_recaps.therapy_recap_subsidy',
                'therapy_recaps.therapy_recap_total_price',
                'queues.id',
                'queues.queue_date',
                'queues.queue_time',
                'queues.queue_patient_id',            
                'queues.queue_status_check',
                'queues.queue_note',
                'patients.patient_fullname',
                'therapies.id',
                'therapies.therapy_name',
            ])->join('queues','queues.id','=','therapy_recaps.therapy_recap_queue_id')
            ->join('therapies','therapies.id','=','therapy_recaps.therapy_recap_therapy_id')
            ->join('patients','patients.id','=','queues.queue_patient_id')
            ->whereRaw('queues.queue_status_check', 1);
        }
        return self::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'therapy_recaps.id',
            'therapy_recaps.therapy_recap_complaint',
            'therapy_recaps.therapy_recap_queue_id',
            'therapy_recaps.therapy_recap_therapy_id',
            'therapy_recaps.therapy_recap_therapist_id',
            'therapy_recaps.therapy_recap_result',
            'therapy_recaps.therapy_recap_receipt',
            'therapy_recaps.therapy_recap_price',
            'therapy_recaps.therapy_recap_subsidy',
            'therapy_recaps.therapy_recap_total_price',
            'queues.id',
            'queues.queue_date',
            'queues.queue_time',
            'queues.queue_patient_id',            
            'queues.queue_status_check',
            'queues.queue_note',
            'patients.patient_fullname',
            'therapies.therapy_name',
        ])->join('queues','queues.id','=','therapy_recaps.therapy_recap_queue_id')
        ->join('therapies','therapies.id','=','therapy_recaps.therapy_recap_therapy_id')
        ->join('patients','patients.id','=','queues.queue_patient_id')  
        ->whereRaw('queues.queue_status_check', 1)
        ->whereRaw('queues.queue_patient_id', $patientId);
    }


    public function getQueue() 
    {
        return $this->hasOne('App\Models\Queue', 'id', 'therapy_recap_queue_id');
    }

    public function getTherapy() 
    {
        return $this->hasOne('App\Models\Therapy', 'id', 'therapy_recap_therapy_id');
    }
    
    public function getTherapist() 
    {
        return $this->hasOne('App\Models\Employee', 'id', 'therapy_recap_therapist_id');
    }
    
}
