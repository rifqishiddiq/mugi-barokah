<?php

Route::get('/', function(){
	return redirect('login');
});

Route::get('daily-report', 'Backend\BackendController@index')->middleware('verify-login')->middleware('not-authenticated');
Route::post('daily-report-daily', 'Backend\BackendController@index2');
Route::get('schedule', 'Backend\BackendController@schedule')->middleware('verify-login');
Route::get('male', 'Backend\BackendController@scheduleMale')->middleware('verify-login')->middleware('not-authenticated');
Route::get('female', 'Backend\BackendController@scheduleFemale')->middleware('verify-login')->middleware('not-authenticated');
Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::group(['prefix' => '/', 'namespace' => 'Backend'], function(){
	Route::get('login', 'LoginController@index');
	Route::get('register', 'LoginController@register');
	Route::get('register2', 'LoginController@register2');
	Route::post('register-save', 'LoginController@save');
	Route::get('logout', 'LoginController@logout')->middleware('revalidate');
	Route::post('validate-login', 'LoginController@login');
	
	Route::get('test', 'BackendController@test');

	Route::get('rekam_medis-pdf/{id}','ExportController@medicalRecordPDF')->middleware('verify-login');
	Route::get('laporan_bulanan-excel/{date}','ExportController@monthlyReportExcel')->middleware('verify-login')->middleware('not-authenticated');

	Route::group(['prefix' => 'chart', 'middleware' => 'verify-login', 'middleware' => 'not-authenticated'], function(){
		Route::get('/', 'BackendController@chart');
		Route::post('date', 'BackendController@chart2');
	});


	Route::group(['prefix' => 'user', 'middleware' => 'verify-login', 'middleware' => 'not-authenticated'], function(){
		Route::get('/', 'UserController@index');
		Route::get('/add/{id}', 'UserController@add');
		Route::get('/detail/{id}', 'UserController@show');
		Route::post('/save', 'UserController@save');
		Route::post('/delete', 'UserController@delete');
		Route::post('/data-table', 'UserController@dataTable');

	});

	Route::group(['prefix' => 'employee', 'middleware' => 'verify-login', 'middleware' => 'not-authenticated'], function(){
		Route::get('/', 'EmployeeController@index');
		Route::get('/add/{id}', 'EmployeeController@add');
		Route::post('/save', 'EmployeeController@save');
		Route::get('/detail/{id}', 'EmployeeController@show');
		Route::post('/data-table', 'EmployeeController@dataTable');
		Route::post('/delete', 'EmployeeController@delete');
	});

	Route::group(['prefix' => 'patient', 'middleware' => 'verify-login', 'middleware' => 'not-authenticated'], function(){
		Route::get('/', 'PatientController@index');
		Route::get('/add/{id}', 'PatientController@add');
		Route::post('/save', 'PatientController@save');
		Route::get('/detail/{id}', 'PatientController@show');
		Route::get('/medical-record/{id}', 'PatientController@medicalRecord');
		Route::post('/data-table', 'PatientController@dataTable');
		Route::get('/chart-gender', 'PatientController@chartGender');
		Route::post('/delete', 'PatientController@delete');
	});

	Route::group(['prefix' => 'profile', 'middleware' => 'verify-login'], function(){
		Route::get('/add/{id}', 'PatientController@add');
		Route::post('/save', 'PatientController@save');
		Route::get('/detail/{id}', 'PatientController@show');
	});

	// Route::group(['prefix' => 'job', 'middleware' => 'verify-login'], function(){
	// 	Route::get('/', 'JobController@index');
	// 	Route::post('/data-table', 'JobController@dataTable');
	// });
	
	// Route::group(['prefix' => 'regency', 'middleware' => 'verify-login'], function(){
	// 	Route::get('/', 'RegencyController@index');
	// 	Route::post('/data-table', 'RegencyController@dataTable');
	// });

	Route::group(['prefix' => 'queue', 'middleware' => 'verify-login'], function(){
		Route::get('/', 'QueueController@index');
		Route::get('/detail/{id}', 'QueueController@detail');
		Route::post('/add', 'QueueController@add');
		Route::post('/save', 'QueueController@save');
		Route::post('/approve', 'QueueController@approve');
		Route::post('/process', 'QueueController@process');
		Route::post('/decline', 'QueueController@decline');
		Route::post('/data-table', 'QueueController@dataTable');
		Route::post('/delete', 'QueueController@delete');
	});

	Route::group(['prefix' => 'medical-record', 'middleware' => 'verify-login'], function(){
		Route::get('/', 'TherapyRecapController@index');
		Route::get('/detail/{id}', 'TherapyRecapController@detail');
		Route::get('/add/{id}', 'TherapyRecapController@add')->middleware('not-authenticated');
		Route::post('/save', 'TherapyRecapController@save')->middleware('not-authenticated');
		Route::get('/data-table', 'TherapyRecapController@dataTable');
		Route::post('/datatable', 'TherapyRecapController@dataTable2');
		Route::post('/delete', 'TherapyRecapController@delete');
	});

});


// Route::get('error', function () {
//     return view('404');
// });


// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
